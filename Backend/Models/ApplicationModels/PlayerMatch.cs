﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Backend.Models
{
    public class PlayerMatch
    {
        public string PlayerId { get; set; }
        public long GameId { get; set; }
        public virtual Match Game { get; set; }
        /*public int Champion { get; set; }
        public string Role { get; set; }
        public string Lane { get; set; }
        public int Queue { get; set; }
        public int Season { get; set; }
        public long Timestamp { get; set; }*/
    }
}
