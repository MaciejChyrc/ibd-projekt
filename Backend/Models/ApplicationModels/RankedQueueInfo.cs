using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Backend.Models
{
    public class RankedQueueInfo
    {
        public string PlayerId { get; set; } //Player's Id, not SummonerId or AccountId
        [Required]
        public string PlayerOrTeamId { get; set; }
        [Required]
        public string PlayerOrTeamName { get; set; }
        [Required]
        public string LeagueId { get; set; }
        [Required]
        public string LeagueName { get; set; }
        [Required]
        public string Tier { get; set; }
        [Required]
        public string Rank { get; set; }
        public int LeaguePoints { get; set; }
        [Required]
        public string QueueType { get; set; }
        public int Wins { get; set; }
        public int Losses { get; set; }
        public bool HotStreak { get; set; }
        public bool FreshBlood { get; set; }
        public bool Veteran { get; set; }
        public bool Inactive { get; set; }
        public int? PromoSeriesWins { get; set; }
        public int? PromoSeriesLosses { get; set; }
        public int? PromoSeriesTarget { get; set; }
        public string PromoSeriesProgress { get; set; }
    }
}