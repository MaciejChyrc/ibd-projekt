using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Backend.Models
{
    public class Match
    {
        [Key]
        public long Id { get; set; }
        public virtual List<TeamStats> Teams { get; set; }
        public int Queue { get; set; }
        public int Season { get; set; }
        public string GameVersion { get; set; } //patch the game was played on
        public string GameMode { get; set; }
        public string GameType { get; set; } //matchmaking, tutorial, custom
        public int MapId { get; set; }
        //datetime and duration given in milliseconds
        public long Timestamp { get; set; } //game creation time (when loading screen appears)
        public long GameDuration { get; set; }
    }
}