using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;

namespace Backend.Models
{
    public class PlayerStats
    {
        [JsonIgnore]
        public long GameId { get; set; }
        [JsonIgnore]
        public int TeamId { get; set; }
        public long SummonerId { get; set; }
        public string SummonerName { get; set; }
        public string Region { get; set; }
        //win, champion played, runes
        public bool Win { get; set; }
        public int ChampionId { get; set; }
        public int? Spell1Id { get; set; }
        public int? Spell2Id { get; set; }
        public int? RunePrimaryTree { get; set; }
        public int? RuneSecondaryTree { get; set; }
        public int? RuneKeystone { get; set; }
        public int? RunePrimary1 { get; set; }
        public int? RunePrimary2 { get; set; }
        public int? RunePrimary3 { get; set; }
        public int? RuneSecondary1 { get; set; }
        public int? RuneSecondary2 { get; set; }
        //KDA
        public int Kills { get; set; }
        public int Deaths { get; set; }
        public int Assists { get; set; }
        //damage dealt/taken
        public long TotalDamageDealtToChampions { get; set; }
        public long PhysicalDamageDealtToChampions { get; set; }
        public long MagicDamageDealtToChampions { get; set; }
        public long TrueDamageDealtToChampions { get; set; }
        public long TotalDamageTaken { get; set; }
        public long PhysicalDamageTaken { get; set; }
        public long MagicDamageTaken { get; set; }
        public long TrueDamageTaken { get; set; }
        //cs, other scores, first and multi kills
        public int TotalMinionsKilled { get; set; } //lane minions
        public int TotalMonstersKilled { get; set; } //jungle monsters
        public int ChampLevel { get; set; }
        public int GoldEarned { get; set; }
        public int WardsPlaced { get; set; }
        public int WardsKilled { get; set; }
        public int VisionWardsBought { get; set; }
        public long VisionScore { get; set; }
        public bool FirstBloodKill { get; set; }
        public bool FirstTowerKill { get; set; }
        public int? DoubleKills { get; set; }
        public int? TripleKills { get; set; }
        public int? QuadraKills { get; set; }
        public int? Pentakills { get; set; }
        //items at the end of the game
        public int? Item0 { get; set; }
        public int? Item1 { get; set; }
        public int? Item2 { get; set; }
        public int? Item3 { get; set; }
        public int? Item4 { get; set; }
        public int? Item5 { get; set; }
        public int? Item6 { get; set; }
    }
}
