using System.Collections.Generic;

namespace Backend.Models
{
    public class LiveMatchParticipant
    {
        public long ChampionId { get; set; }
        public long TeamId { get; set; }
        public long Spell1Id { get; set; }
        public long Spell2Id { get; set; }
        public Player Player { get; set; }
        public List<RankedQueueInfo> RankedQueueInfos { get; set; }
        public List<PlayerStats> LastGamesStats { get; set; }
    }
}