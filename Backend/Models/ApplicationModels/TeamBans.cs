using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;

namespace Backend.Models
{
    public class TeamBans
    {
        [JsonIgnore]
        public long GameId { get; set; }
        [JsonIgnore]
        public int TeamId { get; set; }
        public int PickTurn { get; set; }
        public int ChampionId { get; set; }
    }
}
