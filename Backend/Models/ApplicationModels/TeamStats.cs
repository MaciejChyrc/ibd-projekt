using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;

namespace Backend.Models
{
    public class TeamStats
    {
        [JsonIgnore]
        public long GameId { get; set; }
        public int TeamId { get; set; }
        public virtual List<TeamBans> TeamBans { get; set; }
        public virtual List<PlayerStats> PlayerStats { get; set; }
        public string Win { get; set; }
        public bool FirstBlood { get; set; }
        public bool FirstTower { get; set; }
        public bool FirstDragon { get; set; }
        public bool FirstRiftHerald { get; set; }
        public bool FirstBaron { get; set; }
        public bool FirstInhibitor { get; set; }
        public int TowerKills { get; set; }
        public int DragonKills { get; set; }
        public int RiftHeraldKills { get; set; }
        public int BaronKills { get; set; }
        public int InhibitorKills { get; set; }
        public int VilemawKills { get; set; }
        public int DominionVictoryScore { get; set; }
    }
}