﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Backend.Models
{
    public class UserPlayer
    {
        public string UserId { get; set; }
        public string PlayerId { get; set; }
    }
}
