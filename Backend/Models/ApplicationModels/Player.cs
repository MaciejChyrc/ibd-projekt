﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace Backend.Models
{
    public class Player
    {
        [Key]
        public string Id { get; set; } // SummonerId and AccountId are not unique across regions
        public long SummonerId { get; set; }
        public long AccountId { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Region { get; set; }
        public int ProfileIconId { get; set; }
        public long SummonerLevel { get; set; }
        [JsonIgnore]
        public virtual List<RankedQueueInfo> RankedQueueInfo { get; set; }
        [JsonIgnore]
        public virtual List<PlayerMatch> Matches { get; set; }
    }
}