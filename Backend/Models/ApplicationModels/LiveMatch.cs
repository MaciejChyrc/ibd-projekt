using System.Collections.Generic;
using Backend.Models.CurrentGameInfo;

namespace Backend.Models
{
    public class LiveMatch
    {
        public long GameId { get; set; }
        public long GameStartTime { get; set; }
        public string GameMode { get; set; }
        public long MapId { get; set; }
        public string GameType { get; set; }
        public long QueueType { get; set; }
        public string ObserverKey { get; set; }
        public List<BannedChampion> BannedChampions { get; set; }
        public List<LiveMatchParticipant> Participants { get; set; }
    }
}