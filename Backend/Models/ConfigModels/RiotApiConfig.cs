﻿namespace Backend.Models
{
    public class RiotApiConfig
    {
        public string ApiKey { get; set; }
        public string HeaderParam { get; set; }
    }
}