﻿namespace Backend.Models
{
    public class PlayerDTO
    {
        public string CurrentPlatformId { get; set; }
        public string SummonerName { get; set; }
        public string MatchHistoryUri { get; set; }
        public string PlatformId { get; set; }
        public long CurrentAccountId { get; set; }
        public int ProfileIcon { get; set; }
        public long SummonerId { get; set; }
        public long AccountId { get; set; }
    }
}
