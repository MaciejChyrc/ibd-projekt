﻿namespace Backend.Models
{
    public class ParticipantIdentityDTO
    {
        public PlayerDTO Player { get; set; }
        public int ParticipantId { get; set; }
    }
}
