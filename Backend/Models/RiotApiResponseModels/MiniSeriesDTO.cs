﻿namespace Backend.Models
{
    public class MiniSeriesDTO
    {
        public int Wins { get; set; }
        public int Losses { get; set; }
        public int Target { get; set; }
        public string Progress { get; set; }
    }
}
