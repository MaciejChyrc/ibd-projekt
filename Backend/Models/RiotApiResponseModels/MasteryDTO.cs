﻿namespace Backend.Models
{
    public class MasteryDTO
    {
        public int MasteryId { get; set; }
        public int Rank { get; set; }
    }
}
