namespace Backend.Models.CurrentGameInfo
{
    public class BannedChampion
    {
        public long TeamId { get; set; }
        public long ChampionId { get; set; }
        public int PickTurn { get; set; }
    }
}