using System.Collections.Generic;

namespace Backend.Models.CurrentGameInfo
{
    public class CurrentGameInfo
    {
        public long GameId { get; set; }
        public long GameStartTime { get; set; }
        public string PlatformId { get; set; }
        public string GameMode { get; set; }
        public long MapId { get; set; }
        public string GameType { get; set; }
        public long GameQueueConfigId { get; set; }
        public Observers Observers { get; set; }
        public List<Participant> Participants { get; set; }
        public long GameLength { get; set; }
        public List<BannedChampion> BannedChampions { get; set; }
    }
}
