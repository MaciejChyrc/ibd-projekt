﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Backend.Models
{
    public class SummonerDTO
    {
        public long Id { get; set; }
        public long AccountId { get; set; }
        public long SummonerLevel { get; set; }
        public string Name { get; set; }
        public int ProfileIconId { get; set; }
        public long RevisionDate { get; set; }
    }
}
