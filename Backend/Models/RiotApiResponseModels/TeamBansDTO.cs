﻿namespace Backend.Models
{
    public class TeamBansDTO
    {
        public int PickTurn { get; set; }
        public int ChampionId { get; set; }
    }
}
