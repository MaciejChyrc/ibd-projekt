﻿namespace Backend.Models
{
    public class RuneDTO
    {
        public int RuneId { get; set; }
        public int Rank { get; set; }
    }
}
