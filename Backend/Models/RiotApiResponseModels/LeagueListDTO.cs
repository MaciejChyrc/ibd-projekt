using System.Collections.Generic;

namespace Backend.Models
{
    public class LeagueListDTO
    {
        public string LeagueId { get; set; }
        public string Tier { get; set; }
        public List<LeagueItemDTO> Entries { get; set; }
        public string Queue { get; set; }
        public string Name { get; set; }
    }
}
