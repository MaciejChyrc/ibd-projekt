﻿namespace Backend.Models
{
    public class MatchReferenceDTO
    {
        public long GameId { get; set; }
        public string Lane { get; set; }
        public int Champion { get; set; }
        public string PlatformId { get; set; }
        public int Season { get; set; }
        public int Queue { get; set; }
        public string Role { get; set; }
        public long Timestamp { get; set; }
    }
}
