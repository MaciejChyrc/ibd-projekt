﻿namespace Backend.Models
{
    public class LeaguePositionDTO
    {
        public string Rank { get; set; }
        public string QueueType { get; set; }
        public bool HotStreak { get; set; }
        public MiniSeriesDTO MiniSeries { get; set; }
        public int Wins { get; set; }
        public bool Veteran { get; set; }
        public int Losses { get; set; }
        public bool FreshBlood { get; set; }
        public string LeagueId { get; set; }
        public string PlayerOrTeamName { get; set; }
        public bool Inactive { get; set; }
        public string PlayerOrTeamId { get; set; }
        public string LeagueName { get; set; }
        public string Tier { get; set; }
        public int LeaguePoints { get; set; }
    }
}
