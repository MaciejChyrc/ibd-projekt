﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Models.HttpRequestBodyModels
{
    public class FollowModel
    {
        public string UserName { get; set; }
        public string SummonerName { get; set; }
        public string Region { get; set; }
    }
}
