namespace Backend.Models
{
    public class SummonerIdRegion
    {
        public long SummonerId { get; set; }
        public string Region { get; set; }
    }
}
