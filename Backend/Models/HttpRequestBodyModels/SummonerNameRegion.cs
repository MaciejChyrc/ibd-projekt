﻿namespace Backend.Models
{
    public class SummonerNameRegion
    {
        public string SummonerName { get; set; }
        public string Region { get; set; }
    }
}