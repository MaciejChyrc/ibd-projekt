namespace Backend.Models
{
    public class AccountIdRegion
    {
        public long AccountId { get; set; }
        public string Region { get; set; }
    }
}