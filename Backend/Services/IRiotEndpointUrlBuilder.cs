using System;
using System.Collections.Generic;
using System.Linq;

namespace Backend.Services
{
    public interface IRiotEndpointUrlBuilder
    {
        Uri GetBaseApiUrlWithRegion(string region);
        Uri GetSummonerByName(string summonerName);
        Uri GetMatchListByAccountId(long accountId, int beginIndex, int endIndex);
        Uri GetMatchByMatchId(long matchId);
        Uri GetLeaguePositionsBySummonerId(long summonerId);
        Uri GetChallengerLeagueByQueue(string queue);
        Uri GetLiveMatchBySummonerId(long summonerId);
    }
}
