﻿using Backend.Models;
using Backend.Models.HttpRequestBodyModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Services
{
    public interface IUserDataService
    {
        Task<List<User>> GetUsers();
        Task<bool> AddUser(RegisterModel user);
        Task<User> GetUser(string userId);
        Task<User> GetUserByUsername(string username);
        Task<User> GetUserByEmail(string email);
        Task<bool> FollowPlayer(UserPlayer follow);
        Task<bool> UnfollowPlayer(UserPlayer follow);
        Task<List<Player>> GetFavouritePlayers(User user);
    }
}
