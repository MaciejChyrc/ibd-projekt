﻿using Backend.Models;
using Backend.Models.HttpRequestBodyModels;
using Backend.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Services
{
    public class UserDataService : IUserDataService
    {
        private readonly IUserDataRepository _userDataRepository;
        private readonly IHttpService _httpService;
        private readonly IEntityMapper _entityMapper;

        public UserDataService(IUserDataRepository userDataRepository,
                                 IHttpService httpService,
                                 IEntityMapper entityMapper)
        {
            _userDataRepository = userDataRepository;
            _httpService = httpService;
            _entityMapper = entityMapper;
        }

        private User CreateUser(RegisterModel user)
        {
            var newUser = new User
            {
                Username = user.UserName,
                Email = user.Email,
                Password = user.Password,
                Role = "User",
                EmailConfirmed = false,
                FavouritePlayers = null,
                Id = user.UserName + user.Email
            };

            return newUser;
            
        }

        public async Task<bool> FollowPlayer(UserPlayer follow)
        {
            return await _userDataRepository.FollowPlayer(follow);
        }

        public async Task<bool> UnfollowPlayer(UserPlayer follow)
        {
            return await _userDataRepository.UnfollowPlayer(follow);
        }

        public async Task<List<Player>> GetFavouritePlayers(User user)
        {
            return await _userDataRepository.GetFavouritePlayers(user);
        }

        public async Task<bool> AddUser(RegisterModel user)
        {
            var newUser = CreateUser(user);
            return await _userDataRepository.AddUser(newUser);  
        }

        public async Task<User> GetUser(string userId)
        {
            return await _userDataRepository.GetUser(userId);
        }

        public async Task<User> GetUserByEmail(string email)
        {
            return await _userDataRepository.GetUserByEmail(email);
        }

        public async Task<User> GetUserByUsername(string username)
        {
            return await _userDataRepository.GetUserByUsername(username);
        }

        public async Task<List<User>> GetUsers()
        {
            return await _userDataRepository.GetUsers();
        }
    }
}
