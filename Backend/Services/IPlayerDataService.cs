using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Backend.Models;

namespace Backend.Services
{
    public interface IPlayerDataService
    {
        Task<(Player, HttpStatusCode)> GetPlayer(string summonerName, string region);
        Task<(Player, HttpStatusCode)> FetchSavePlayer(string summonerName, string region);
        Task<(List<RankedQueueInfo>, HttpStatusCode)> GetPlayerRankedInfo(long summonerId, string region);
        Task<(List<RankedQueueInfo>, HttpStatusCode)> FetchPlayerRankedInfo(long summonerId, string region);
        Task<(List<Match>, HttpStatusCode)> GetRecentMatches(long accountId, string region, int count);
        Task<(List<Match>, HttpStatusCode)> FetchRecentMatches(long accountId, string region, int count);
        Task<(LiveMatch, HttpStatusCode)> GetLiveMatchData(long summonerId, string region);
    }
}
