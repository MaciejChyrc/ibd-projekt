using System;
using System.Collections.Generic;
using System.Linq;
using Backend.Models;
using Backend.Models.CurrentGameInfo;

namespace Backend.Services
{
    public class EntityMapper : IEntityMapper
    {
        public EntityMapper()
        {

        }

        public Player SummonerDTOToPlayer(SummonerDTO summonerDto, string region)
        {
            return new Player
            {
                SummonerId = summonerDto.Id,
                AccountId = summonerDto.AccountId,
                Name = summonerDto.Name,
                Region = region,
                ProfileIconId = summonerDto.ProfileIconId,
                SummonerLevel = summonerDto.SummonerLevel
            };
        }

        public RankedQueueInfo LeaguePositionDTOToRankedQueueInfo(LeaguePositionDTO leaguePositionDto, string playerId)
        {
            return new RankedQueueInfo
            {
                PlayerId = playerId,
                PlayerOrTeamId = leaguePositionDto.PlayerOrTeamId,
                PlayerOrTeamName = leaguePositionDto.PlayerOrTeamName,
                LeagueId = leaguePositionDto.LeagueId,
                LeagueName = leaguePositionDto.LeagueName,
                Tier = leaguePositionDto.Tier,
                Rank = leaguePositionDto.Rank,
                LeaguePoints = leaguePositionDto.LeaguePoints,
                QueueType = leaguePositionDto.QueueType,
                Wins = leaguePositionDto.Wins,
                Losses = leaguePositionDto.Losses,
                HotStreak = leaguePositionDto.HotStreak,
                FreshBlood = leaguePositionDto.FreshBlood,
                Veteran = leaguePositionDto.Veteran,
                Inactive = leaguePositionDto.Inactive,
                PromoSeriesWins = (leaguePositionDto.MiniSeries != null ? (int?)leaguePositionDto.MiniSeries.Wins : null),
                PromoSeriesLosses = (leaguePositionDto.MiniSeries != null ? (int?)leaguePositionDto.MiniSeries.Losses : null),
                PromoSeriesTarget = (leaguePositionDto.MiniSeries != null ? (int?)leaguePositionDto.MiniSeries.Target : null),
                PromoSeriesProgress = (leaguePositionDto.MiniSeries != null ? leaguePositionDto.MiniSeries.Progress : null)
            };
        }

        public Match MatchDTOMatchReferenceDTOToMatch(MatchReferenceDTO matchReferenceDto, MatchDTO matchDto)
        {
            return new Match
            {
                Id = matchReferenceDto.GameId,
                Queue = matchReferenceDto.Queue,
                Season = matchReferenceDto.Season,
                GameVersion = matchDto.GameVersion,
                GameMode = matchDto.GameMode,
                GameType = matchDto.GameType,
                MapId = matchDto.MapId,
                Timestamp = matchReferenceDto.Timestamp,
                GameDuration = matchDto.GameDuration
            };
        }

        public TeamStats TeamStatsDTOToTeamStats(TeamStatsDTO teamStatsDto, long gameId)
        {
            return new TeamStats
            {
                GameId = gameId,
                TeamId = teamStatsDto.TeamId,
                Win = teamStatsDto.Win,
                FirstBlood = teamStatsDto.FirstBlood,
                FirstTower = teamStatsDto.FirstTower,
                FirstDragon = teamStatsDto.FirstDragon,
                FirstRiftHerald = teamStatsDto.FirstRiftHerald,
                FirstBaron = teamStatsDto.FirstBaron,
                FirstInhibitor = teamStatsDto.FirstInhibitor,
                TowerKills = teamStatsDto.TowerKills,
                DragonKills = teamStatsDto.DragonKills,
                RiftHeraldKills = teamStatsDto.RiftHeraldKills,
                BaronKills = teamStatsDto.BaronKills,
                InhibitorKills = teamStatsDto.InhibitorKills,
                VilemawKills = teamStatsDto.VilemawKills,
                DominionVictoryScore = teamStatsDto.DominionVictoryScore
            };
        }

        public TeamBans TeamBansDTOToTeamBans(TeamBansDTO teamBansDto, long gameId, int teamId)
        {
            return new TeamBans
            {
                GameId = gameId,
                TeamId = teamId,
                PickTurn = teamBansDto.PickTurn,
                ChampionId = teamBansDto.ChampionId
            };
        }

        public PlayerStats ParticipantDTOToPlayerStats(
            ParticipantDTO participantDto,
            long gameId,
            long summonerId,
            string summonerName,
            string region)
        {
            return new PlayerStats
            {
                GameId = gameId,
                TeamId = participantDto.TeamId,
                SummonerId = summonerId,
                SummonerName = summonerName,
                Region = region,
                Win = participantDto.Stats.Win,
                ChampionId = participantDto.ChampionId,
                Spell1Id = participantDto.Spell1Id,
                Spell2Id = participantDto.Spell2Id,
                RunePrimaryTree = participantDto.Stats.PerkPrimaryStyle,
                RuneSecondaryTree = participantDto.Stats.PerkSubStyle,
                RuneKeystone = participantDto.Stats.Perk0,
                RunePrimary1 = participantDto.Stats.Perk1,
                RunePrimary2 = participantDto.Stats.Perk2,
                RunePrimary3 = participantDto.Stats.Perk3,
                RuneSecondary1 = participantDto.Stats.Perk4,
                RuneSecondary2 = participantDto.Stats.Perk5,
                Kills = participantDto.Stats.Kills,
                Deaths = participantDto.Stats.Deaths,
                Assists = participantDto.Stats.Assists,
                TotalDamageDealtToChampions = participantDto.Stats.TotalDamageDealtToChampions,
                PhysicalDamageDealtToChampions = participantDto.Stats.PhysicalDamageDealtToChampions,
                MagicDamageDealtToChampions = participantDto.Stats.MagicDamageDealtToChampions,
                TrueDamageDealtToChampions = participantDto.Stats.TrueDamageDealtToChampions,
                TotalDamageTaken = participantDto.Stats.TotalDamageTaken,
                PhysicalDamageTaken = participantDto.Stats.PhysicalDamageTaken,
                MagicDamageTaken = participantDto.Stats.MagicalDamageTaken,
                TrueDamageTaken = participantDto.Stats.TrueDamageTaken,
                TotalMinionsKilled = participantDto.Stats.TotalMinionsKilled,
                TotalMonstersKilled = participantDto.Stats.NeutralMinionsKilled,
                ChampLevel = participantDto.Stats.ChampLevel,
                GoldEarned = participantDto.Stats.GoldEarned,
                WardsPlaced = participantDto.Stats.WardsPlaced,
                WardsKilled = participantDto.Stats.WardsKilled,
                VisionWardsBought = participantDto.Stats.VisionWardsBoughtInGame,
                VisionScore = participantDto.Stats.VisionScore,
                FirstBloodKill = participantDto.Stats.FirstBloodKill,
                FirstTowerKill = participantDto.Stats.FirstTowerKill,
                DoubleKills = participantDto.Stats.DoubleKills,
                TripleKills = participantDto.Stats.TripleKills,
                QuadraKills = participantDto.Stats.QuadraKills,
                Pentakills = participantDto.Stats.PentaKills,
                Item0 = participantDto.Stats.Item0,
                Item1 = participantDto.Stats.Item1,
                Item2 = participantDto.Stats.Item2,
                Item3 = participantDto.Stats.Item3,
                Item4 = participantDto.Stats.Item4,
                Item5 = participantDto.Stats.Item5,
                Item6 = participantDto.Stats.Item6
            };
        }

        public List<TeamStats> MapTeamStatsList(List<TeamStatsDTO> teamStatsDtos, long gameId)
        {
            var teamStats = new List<TeamStats>();

            foreach (var teamStatsDto in teamStatsDtos)
            {
                teamStats.Add(TeamStatsDTOToTeamStats(teamStatsDto, gameId));
            }

            return teamStats;
        }

        public List<TeamBans> MapTeamBansList(List<TeamBansDTO> teamBansDtos, long gameId, int teamId)
        {
            var teamBans = new List<TeamBans>();

            foreach (var teamBan in teamBansDtos)
            {
                teamBans.Add(TeamBansDTOToTeamBans(teamBan, gameId, teamId));
            }

            return teamBans;
        }

        public List<PlayerStats> MapPlayerStatsList(
            List<ParticipantIdentityDTO> participantIdentityDtos,
            List<ParticipantDTO> participantDtos,
            long gameId,
            string region)
        {
            var playerStats = new List<PlayerStats>();

            foreach (var participantIdentityDto in participantIdentityDtos)
            {
                var participantDto = participantDtos
                    .Where(x => x.ParticipantId == participantIdentityDto.ParticipantId).First();
                playerStats.Add(ParticipantDTOToPlayerStats(
                    participantDto,
                    gameId,
                    participantIdentityDto.Player.SummonerId,
                    participantIdentityDto.Player.SummonerName,
                    region));
            }

            return playerStats;
        }

        public LiveMatch CurrentGameInfoToLiveMatch(CurrentGameInfo cgi, List<LiveMatchParticipant> participants)
        {
            return new LiveMatch
            {
                GameId = cgi.GameId,
                GameStartTime = cgi.GameStartTime,
                GameMode = cgi.GameMode,
                MapId = cgi.MapId,
                GameType = cgi.GameType,
                QueueType = cgi.GameQueueConfigId,
                ObserverKey = cgi.Observers.EncryptionKey,
                BannedChampions = cgi.BannedChampions,
                Participants = participants
            };
        }
    }
}
