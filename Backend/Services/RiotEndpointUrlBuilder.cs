using System;
using System.Collections.Generic;
using System.Linq;

namespace Backend.Services
{
    public class RiotEndpointUrlBuilder : IRiotEndpointUrlBuilder
    {
        public Uri GetBaseApiUrlWithRegion(string region) => new Uri("https://" + region + ".api.riotgames.com/");
        public Uri GetSummonerByName(string summonerName) => new Uri("lol/summoner/v3/summoners/by-name/" + summonerName, UriKind.Relative);
        public Uri GetMatchListByAccountId(long accountId, int beginIndex, int endIndex) =>
            new Uri("lol/match/v3/matchlists/by-account/" +
                    accountId + "?beginIndex=" + beginIndex + "&endIndex=" + endIndex,
                    UriKind.Relative);
        public Uri GetMatchByMatchId(long matchId) => new Uri("lol/match/v3/matches/" + matchId, UriKind.Relative);
        public Uri GetLeaguePositionsBySummonerId(long summonerId) => new Uri("lol/league/v3/positions/by-summoner/" + summonerId, UriKind.Relative);
        //valid queue strings as of 26/09/18: RANKED_SOLO_5x5, RANKED_FLEX_SR, RANKED_FLEX_TT
        public Uri GetChallengerLeagueByQueue(string queue) => new Uri("lol/league/v3/challengerleagues/by-queue/" + queue, UriKind.Relative);
        public Uri GetLiveMatchBySummonerId(long summonerId) => new Uri("lol/spectator/v3/active-games/by-summoner/" + summonerId, UriKind.Relative);
    }
}
