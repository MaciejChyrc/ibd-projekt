using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using Microsoft.EntityFrameworkCore;
using Backend.Models;
using Backend.Repositories;
using Backend.Services;
using System.Threading.Tasks;

namespace Backend.Services
{
    public class PlayerDataService : IPlayerDataService
    {
        private readonly IPlayerDataRepository _playerDataRepository;
        private readonly IHttpService _httpService;
        private readonly IEntityMapper _entityMapper;

        public PlayerDataService(IPlayerDataRepository playerDataRepository,
                                 IHttpService httpService,
                                 IEntityMapper entityMapper)
        {
            _playerDataRepository = playerDataRepository;
            _httpService = httpService;
            _entityMapper = entityMapper;
        }

        public async Task<(Player, HttpStatusCode)> GetPlayer(string summonerName, string region)
        {
            var player = await _playerDataRepository.GetPlayerBySummonerNameAndRegion(summonerName, region);

            return (player != null) ? (player, HttpStatusCode.OK) : (null, HttpStatusCode.NotFound);
        }

        public async Task<(Player, HttpStatusCode)> FetchSavePlayer(string summonerName, string region)
        {
            Player player = null;
            var riotResponse = await _httpService.GetSummonerByName(summonerName, region);

            if (riotResponse.Item2 == HttpStatusCode.OK)
            {
                player = await _playerDataRepository.GetPlayerBySummonerIdAndRegion(riotResponse.Item1.Id, region);

                if (player == null)
                {
                    player = _entityMapper.SummonerDTOToPlayer(riotResponse.Item1, region);
                    return (await _playerDataRepository.AddPlayer(player)) ?
                        (player, HttpStatusCode.OK) :
                        (null, HttpStatusCode.InternalServerError);
                }
                else
                {
                    var updatedPlayer = _entityMapper.SummonerDTOToPlayer(riotResponse.Item1, region);
                    return (await _playerDataRepository.UpdatePlayer(player, updatedPlayer)) ?
                        (updatedPlayer, HttpStatusCode.OK) :
                        (null, HttpStatusCode.InternalServerError);
                }
            }

            return (null, riotResponse.Item2);
        }

        public async Task<(List<RankedQueueInfo>, HttpStatusCode)> GetPlayerRankedInfo(long summonerId, string region)
        {
            var player = await _playerDataRepository.GetPlayerBySummonerIdAndRegion(summonerId, region);

            if (player == null)
                return (null, HttpStatusCode.NotFound);

            var rankedQueueInfos = await _playerDataRepository.GetRankedQueueInfosByPlayerId(player.Id);

            return (rankedQueueInfos == null || rankedQueueInfos.Count == 0) ?
                (null, HttpStatusCode.NotFound) :
                (rankedQueueInfos, HttpStatusCode.OK);
        }

        public async Task<(List<RankedQueueInfo>, HttpStatusCode)> FetchPlayerRankedInfo(long summonerId, string region)
        {
            List<RankedQueueInfo> existingRankedQueueInfos = null;
            var riotResponse = await _httpService.GetLeaguePositionsBySummonerId(summonerId, region);

            if (riotResponse.Item2 == HttpStatusCode.OK)
            {
                var player = await _playerDataRepository.GetPlayerBySummonerIdAndRegion(summonerId, region);

                if (player == null)
                    return (null, HttpStatusCode.NotFound); //return 404

                var rankedQueueInfos = new List<RankedQueueInfo>();
                riotResponse.Item1.ForEach(x =>
                {
                    rankedQueueInfos.Add(_entityMapper.LeaguePositionDTOToRankedQueueInfo(x, player.Id));
                });
                existingRankedQueueInfos = await _playerDataRepository.GetRankedQueueInfosByPlayerId(player.Id);

                if (existingRankedQueueInfos == null || existingRankedQueueInfos.Count <= 0)
                {
                    foreach (var x in rankedQueueInfos)
                    {
                        await _playerDataRepository.AddRankedQueueInfo(x);
                    }
                }
                else
                {
                    foreach (var x in rankedQueueInfos)
                    {
                        if (!existingRankedQueueInfos.Exists(y =>
                                                     y.PlayerId.ToUpper() == x.PlayerId.ToUpper() &&
                                                     y.QueueType.ToUpper() == x.QueueType.ToUpper()))
                            await _playerDataRepository.AddRankedQueueInfo(x);
                        else
                        {
                            var existing = existingRankedQueueInfos.Where(y =>
                                                     y.PlayerId.ToUpper() == x.PlayerId.ToUpper() &&
                                                     y.QueueType.ToUpper() == x.QueueType.ToUpper()).Single();
                            await _playerDataRepository.UpdateRankedQueueInfo(existing, x);
                        }
                    }
                }

                return (rankedQueueInfos, HttpStatusCode.OK);
            }

            return (null, riotResponse.Item2);
        }

        public async Task<(List<Match>, HttpStatusCode)> GetRecentMatches(long accountId, string region, int count)
        {
            var player = await _playerDataRepository.GetPlayerByAccountIdAndRegion(accountId, region);

            if (player == null)
                return (null, HttpStatusCode.NotFound);

            var matches = await _playerDataRepository.GetRecentMatchesByPlayerId(player.Id, count);

            return (matches == null || matches.Count == 0) ?
                (null, HttpStatusCode.NotFound) :
                (matches, HttpStatusCode.OK);
        }

        public async Task<(List<Match>, HttpStatusCode)> FetchRecentMatches(long accountId, string region, int count)
        {
            var matchListResponse = await _httpService.GetMatchListByAccountId(accountId, region, 0, count);

            if (matchListResponse.Item2 == HttpStatusCode.OK)
            {
                var player = await _playerDataRepository.GetPlayerByAccountIdAndRegion(accountId, region);

                if (player == null)
                    return (null, HttpStatusCode.NotFound);

                var existingPlayerMatchIds = await _playerDataRepository.GetPlayerMatchIdsByPlayerId(player.Id);
                var existingMatchGameIds = await _playerDataRepository.GetMatchIdsInList(matchListResponse.Item1.Matches.Select(x => x.GameId).ToList());
                var newMatches = matchListResponse.Item1.Matches
                    .Where(x => !existingMatchGameIds.Contains(x.GameId));
                var newPlayerMatchGameIds = existingMatchGameIds
                    .Where(x => !existingPlayerMatchIds.Contains(x));

                foreach (var matchReferenceDto in newMatches)
                {
                    var matchResponse = await _httpService.GetMatchByMatchId(matchReferenceDto.GameId, region);
                    bool addMatchSuccess = false;

                    if (matchResponse.Item2 == HttpStatusCode.OK)
                    {
                        var gameId = matchResponse.Item1.GameId;
                        var match = _entityMapper.MatchDTOMatchReferenceDTOToMatch(matchReferenceDto, matchResponse.Item1);
                        var teams = _entityMapper.MapTeamStatsList(matchResponse.Item1.Teams, gameId);

                        foreach (var team in teams)
                        {
                            var bansDtos = matchResponse.Item1.Teams.Where(x => x.TeamId == team.TeamId).Single().Bans;
                            team.TeamBans = _entityMapper.MapTeamBansList(bansDtos, gameId, team.TeamId);
                            team.PlayerStats = _entityMapper
                                .MapPlayerStatsList(
                                    matchResponse.Item1.ParticipantIdentities,
                                    matchResponse.Item1.Participants,
                                    gameId,
                                    region).Where(x => x.TeamId == team.TeamId).ToList();
                        }

                        match.Teams = teams;
                        var playerMatch = new PlayerMatch
                        {
                            PlayerId = player.Id,
                            GameId = gameId,
                            Game = match
                        };
                        addMatchSuccess = await _playerDataRepository.AddPlayerMatch(playerMatch);
                    }
                }
                foreach (var newPlayerMatchGameId in newPlayerMatchGameIds)
                {
                    var playerMatch = new PlayerMatch
                    {
                        PlayerId = player.Id,
                        GameId = newPlayerMatchGameId
                    };
                    await _playerDataRepository.AddPlayerMatch(playerMatch);
                }

                var matches = await _playerDataRepository.GetRecentMatchesByPlayerId(player.Id, count);

                return matches.Count > 0 ? (matches, HttpStatusCode.OK) : (null, HttpStatusCode.InternalServerError);
            }

            return (null, matchListResponse.Item2); //return riot api's status code
        }

        public async Task<(LiveMatch, HttpStatusCode)> GetLiveMatchData(long summonerId, string region)
        {
            var currentGameInfoResponse = await _httpService.GetCurrentGameInfoBySummonerId(summonerId, region);

            if (currentGameInfoResponse.Item2 == HttpStatusCode.OK)
            {
                var participants = new List<LiveMatchParticipant>();

                foreach (var participantRiot in currentGameInfoResponse.Item1.Participants)
                {
                    var playerResponse = await GetPlayer(participantRiot.SummonerName, region);

                    if (playerResponse.Item1 == null)
                        playerResponse = await FetchSavePlayer(participantRiot.SummonerName, region);
                    if (playerResponse.Item1 != null)
                    {
                        var rqiResponse = await FetchPlayerRankedInfo(playerResponse.Item1.SummonerId, region);
                        var matchesResponse = await FetchRecentMatches(playerResponse.Item1.AccountId, region, 3);

                        var statsList = new List<PlayerStats>();
                        if (matchesResponse.Item1 != null)
                        {
                            foreach (var match in matchesResponse.Item1)
                            {
                                var stats = GetPlayerStatsFromMatch(
                                                playerResponse.Item1.SummonerId,
                                                playerResponse.Item1.Region,
                                                match);
                                if (stats != null)
                                    statsList.Add(stats);
                            }
                        }

                        participants.Add(
                            new LiveMatchParticipant
                            {
                                ChampionId = participantRiot.ChampionId,
                                TeamId = participantRiot.TeamId,
                                Spell1Id = participantRiot.Spell1Id,
                                Spell2Id = participantRiot.Spell2Id,
                                Player = playerResponse.Item1,
                                RankedQueueInfos = rqiResponse.Item1,
                                LastGamesStats = statsList
                            });

                        await Task.Delay(200);
                    }
                }

                var liveMatch = _entityMapper.CurrentGameInfoToLiveMatch(currentGameInfoResponse.Item1, participants);
                return (liveMatch, HttpStatusCode.OK);
            }

            return (null, currentGameInfoResponse.Item2);
        }

        public PlayerStats GetPlayerStatsFromMatch(long summonerId, string region, Match match)
        {
            foreach (var team in match.Teams)
            {
                var playerStats = team.PlayerStats
                                    .Where(x => x.SummonerId == summonerId && x.Region.ToUpper() == region.ToUpper())
                                    .SingleOrDefault();

                if (playerStats != null)
                    return playerStats;
            }

            return null;
        }

        public async Task<bool> MapSaveMatch(MatchReferenceDTO matchReferenceDto, MatchDTO matchDto)
        {
            var match = _entityMapper.MatchDTOMatchReferenceDTOToMatch(matchReferenceDto, matchDto);
            return await _playerDataRepository.AddMatch(match);
        }
    }
}
