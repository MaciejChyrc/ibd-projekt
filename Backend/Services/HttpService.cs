using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using RestSharp;
using Backend.Models;
using Microsoft.Extensions.Options;
using System.Net;
using Backend.Models.CurrentGameInfo;

namespace Backend.Services
{
    public class HttpService : IHttpService
    {
        private readonly RiotApiConfig _riotApiConfig;
        private readonly IRiotEndpointUrlBuilder _riotEndpointBuilder;
        private readonly ILogger<HttpService> _logger;
        private RestClient _restClient;

        public HttpService(IOptions<RiotApiConfig> riotApiConfig,
                           IRiotEndpointUrlBuilder riotEndpointUrlBuilder,
                           ILogger<HttpService> logger)
        {
            _riotApiConfig = riotApiConfig.Value;
            _riotEndpointBuilder = riotEndpointUrlBuilder;
            _logger = logger;
            _restClient = new RestClient();
        }

        public async Task<(SummonerDTO, HttpStatusCode)> GetSummonerByName(string summonerName, string region)
        {
            _restClient.BaseUrl = _riotEndpointBuilder.GetBaseApiUrlWithRegion(region);
            var request = new RestRequest(_riotEndpointBuilder.GetSummonerByName(summonerName), Method.GET);
            request.AddHeader(_riotApiConfig.HeaderParam, _riotApiConfig.ApiKey);
            var response = await _restClient.ExecuteTaskAsync(request);

            if (response.IsSuccessful)
            {
                _logger.LogDebug(response.Content + "\n" + response.ContentType + " | " + response.ContentEncoding);
                var summoner = JsonConvert.DeserializeObject<SummonerDTO>(response.Content);
                return (summoner, response.StatusCode);
            }

            return (null, response.StatusCode);
        }

        public async Task<(List<LeaguePositionDTO>, HttpStatusCode)> GetLeaguePositionsBySummonerId(long summonerId, string region)
        {
            _restClient.BaseUrl = _riotEndpointBuilder.GetBaseApiUrlWithRegion(region);
            var request = new RestRequest(_riotEndpointBuilder.GetLeaguePositionsBySummonerId(summonerId), Method.GET);
            request.AddHeader(_riotApiConfig.HeaderParam, _riotApiConfig.ApiKey);
            var response = await _restClient.ExecuteTaskAsync(request);

            if (response.IsSuccessful)
            {
                _logger.LogDebug(response.Content + "\n" + response.ContentType + " | " + response.ContentEncoding);
                var leaguePositions = JsonConvert.DeserializeObject<List<LeaguePositionDTO>>(response.Content);
                return (leaguePositions, response.StatusCode);
            }

            return (null, response.StatusCode);
        }

        public async Task<(MatchlistDTO, HttpStatusCode)> GetMatchListByAccountId(long accountId, string region, int beginIndex, int endIndex)
        {
            _restClient.BaseUrl = _riotEndpointBuilder.GetBaseApiUrlWithRegion(region);
            var request = new RestRequest(_riotEndpointBuilder.GetMatchListByAccountId(accountId, beginIndex, endIndex), Method.GET);
            request.AddHeader(_riotApiConfig.HeaderParam, _riotApiConfig.ApiKey);
            var response = await _restClient.ExecuteTaskAsync(request);

            if (response.IsSuccessful)
            {
                _logger.LogDebug(response.Content + "\n" + response.ContentType + " | " + response.ContentEncoding);
                var matchList = JsonConvert.DeserializeObject<MatchlistDTO>(response.Content);
                return (matchList, response.StatusCode);
            }

            return (null, response.StatusCode);
        }

        public async Task<(MatchDTO, HttpStatusCode)> GetMatchByMatchId(long matchId, string region)
        {
            _restClient.BaseUrl = _riotEndpointBuilder.GetBaseApiUrlWithRegion(region);
            var request = new RestRequest(_riotEndpointBuilder.GetMatchByMatchId(matchId), Method.GET);
            request.AddHeader(_riotApiConfig.HeaderParam, _riotApiConfig.ApiKey);
            var response = await _restClient.ExecuteTaskAsync(request);

            if (response.IsSuccessful)
            {
                _logger.LogDebug(response.Content + "\n" + response.ContentType + " | " + response.ContentEncoding);
                var match = JsonConvert.DeserializeObject<MatchDTO>(response.Content);
                return (match, response.StatusCode);
            }

            return (null, response.StatusCode);
        }

        public async Task<(LeagueListDTO, HttpStatusCode)> GetChallengerLeagueByQueue(string queue, string region)
        {
            _restClient.BaseUrl = _riotEndpointBuilder.GetBaseApiUrlWithRegion(region);
            var request = new RestRequest(_riotEndpointBuilder.GetChallengerLeagueByQueue(queue), Method.GET);
            request.AddHeader(_riotApiConfig.HeaderParam, _riotApiConfig.ApiKey);
            var response = await _restClient.ExecuteTaskAsync(request);

            if (response.IsSuccessful)
            {
                _logger.LogDebug(response.Content + "\n" + response.ContentType + " | " + response.ContentEncoding);
                var leagueList = JsonConvert.DeserializeObject<LeagueListDTO>(response.Content);
                return (leagueList, response.StatusCode);
            }

            return (null, response.StatusCode);
        }

        public async Task<(CurrentGameInfo, HttpStatusCode)> GetCurrentGameInfoBySummonerId(long summonerId, string region)
        {
            _restClient.BaseUrl = _riotEndpointBuilder.GetBaseApiUrlWithRegion(region);
            var request = new RestRequest(_riotEndpointBuilder.GetLiveMatchBySummonerId(summonerId), Method.GET);
            request.AddHeader(_riotApiConfig.HeaderParam, _riotApiConfig.ApiKey);
            var response = await _restClient.ExecuteTaskAsync(request);

            if (response.IsSuccessful)
            {
                _logger.LogDebug(response.Content + "\n" + response.ContentType + " | " + response.ContentEncoding);
                var liveMatch = JsonConvert.DeserializeObject<CurrentGameInfo>(response.Content);
                return (liveMatch, response.StatusCode);
            }

            return (null, response.StatusCode);
        }
    }
}
