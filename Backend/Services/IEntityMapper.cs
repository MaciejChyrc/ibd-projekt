using System;
using System.Collections.Generic;
using System.Linq;
using Backend.Models;
using Backend.Models.CurrentGameInfo;

namespace Backend.Services
{
    public interface IEntityMapper
    {
        Player SummonerDTOToPlayer(SummonerDTO summonerDto, string region);
        RankedQueueInfo LeaguePositionDTOToRankedQueueInfo(LeaguePositionDTO leaguePositionDto, string playerId);
        Match MatchDTOMatchReferenceDTOToMatch(MatchReferenceDTO matchReferenceDto, MatchDTO matchDto);
        TeamStats TeamStatsDTOToTeamStats(TeamStatsDTO teamStatsDto, long gameId);
        TeamBans TeamBansDTOToTeamBans(TeamBansDTO teamBansDto, long gameId, int teamId);
        PlayerStats ParticipantDTOToPlayerStats(ParticipantDTO participantDto, long gameId, long summonerId, string summonerName, string region);
        List<TeamStats> MapTeamStatsList(List<TeamStatsDTO> teamStatsDtos, long gameId);
        List<TeamBans> MapTeamBansList(List<TeamBansDTO> teamBansDtos, long gameId, int teamId);
        List<PlayerStats> MapPlayerStatsList(
            List<ParticipantIdentityDTO> participantIdentityDtos,
            List<ParticipantDTO> participantDtos,
            long gameId,
            string region);
        LiveMatch CurrentGameInfoToLiveMatch(CurrentGameInfo cgi, List<LiveMatchParticipant> participants);
    }
}
