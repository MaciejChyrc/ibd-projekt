using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Backend.Models;
using Backend.Models.CurrentGameInfo;

namespace Backend.Services
{
    public interface IHttpService
    {
        Task<(SummonerDTO, HttpStatusCode)> GetSummonerByName(string summonerName, string region);
        Task<(List<LeaguePositionDTO>, HttpStatusCode)> GetLeaguePositionsBySummonerId(long summonerId, string region);
        Task<(MatchlistDTO, HttpStatusCode)> GetMatchListByAccountId(long accountId, string region, int beginIndex, int endIndex);
        Task<(MatchDTO, HttpStatusCode)> GetMatchByMatchId(long matchId, string region);
        Task<(LeagueListDTO, HttpStatusCode)> GetChallengerLeagueByQueue(string queue, string region);
        Task<(CurrentGameInfo, HttpStatusCode)> GetCurrentGameInfoBySummonerId(long summonerId, string region);
    }
}
