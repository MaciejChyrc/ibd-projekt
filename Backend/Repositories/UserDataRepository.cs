﻿using Backend.Data;
using Backend.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Repositories
{
    public class UserDataRepository : IUserDataRepository
    {
        private readonly ApiDbContext _context;
        private readonly ILogger<UserDataRepository> _logger;

        public UserDataRepository(ApiDbContext context, ILogger<UserDataRepository> logger)
        {
            _context = context;
            _logger = logger;
        }

        public async Task<List<User>> GetUsers()
        {
            return await _context.User.ToListAsync();
        }

        public async Task<bool> AddUser(User user)
        {
            try
            {
                _context.Add(user);
                await _context.SaveChangesAsync();
                return true;
            }
            catch (Exception e) when (e is DbUpdateConcurrencyException ||
                                      e is DbUpdateException ||
                                      e is InvalidOperationException)
            {
                _logger.LogError(e, "Failed to add a user.");
                return false;
            }
        }

        public async Task<bool> FollowPlayer(UserPlayer follow)
        {
            _logger.LogInformation("Following: " + follow.UserId + " " + follow.PlayerId);
            try
            {
                _context.Add(follow);
                await _context.SaveChangesAsync();
                return true;
            }
            catch (Exception e) when (e is DbUpdateConcurrencyException ||
                                      e is DbUpdateException ||
                                      e is InvalidOperationException)
            {
                _logger.LogError(e, "Failed to follow.");
                return false;
            }
        }

        public async Task<bool> UnfollowPlayer(UserPlayer follow)
        {
            _logger.LogInformation("Unfollowing: " + follow.UserId + " " + follow.PlayerId);
            try
            {
                _context.Remove(follow);
                await _context.SaveChangesAsync();
                return true;
            }
            catch (Exception e) when (e is DbUpdateConcurrencyException ||
                                      e is DbUpdateException ||
                                      e is InvalidOperationException)
            {
                _logger.LogError(e, "Failed to unfollow.");
                return false;
            }
        }

        public async Task<List<Player>> GetFavouritePlayers(User user)
        {
            var myUser = await this.GetUserByUsername(user.Username);
            var usersFollows = await _context.UserPlayer.Where(follow => follow.UserId == user.Id).ToListAsync();
            var followedPlayersIds = new List<string>();
            foreach (var follow in usersFollows)
            {
                followedPlayersIds.Add(follow.PlayerId);
            }
            return await _context.Player.Where(player => followedPlayersIds.Contains(player.Id)).ToListAsync();
        }

        public async Task<User> GetUser(string userId)
        {
            return await _context.User.Where(user => user.Id == userId).SingleOrDefaultAsync();
        }

        public async Task<User> GetUserByUsername(string username)
        {
            return await _context.User.Where(user => user.Username == username).SingleOrDefaultAsync();
        }

        public async Task<User> GetUserByEmail(string email)
        {
            return await _context.User.Where(user => user.Email == email).SingleOrDefaultAsync();
        }
    }
}
