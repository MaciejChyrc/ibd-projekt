using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Backend.Data;
using Backend.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace Backend.Repositories
{
    public class PlayerDataRepository : IPlayerDataRepository
    {
        private readonly ApiDbContext _context;
        private readonly ILogger<PlayerDataRepository> _logger;

        public PlayerDataRepository(ApiDbContext context, ILogger<PlayerDataRepository> logger)
        {
            _context = context;
            _logger = logger;
        }

        public async Task<Player> GetPlayerBySummonerNameAndRegion(string summonerName, string region)
        {
            return await _context.Player.Where(x => x.Name.ToUpper() == summonerName.ToUpper() && x.Region.ToUpper() == region.ToUpper()).SingleOrDefaultAsync();
        }

        public async Task<Player> GetPlayerBySummonerIdAndRegion(long summonerId, string region)
        {
            return await _context.Player.Where(x => x.SummonerId == summonerId && x.Region.ToUpper() == region.ToUpper()).SingleOrDefaultAsync();
        }

        public async Task<Player> GetPlayerByAccountIdAndRegion(long accountId, string region)
        {
            return await _context.Player.Where(x => x.AccountId == accountId && x.Region.ToUpper() == region.ToUpper()).SingleOrDefaultAsync();
        }

        public async Task<List<Match>> GetRecentMatchesByPlayerId(string playerId, int count)
        {
            return await _context.PlayerMatch
                .Where(x => x.PlayerId == playerId)
                .OrderByDescending(x => x.Game.Timestamp)
                .Take(count)
                .Select(x => x.Game)
                .Include(x => x.Teams).ThenInclude(x => x.PlayerStats)
                .Include(x => x.Teams).ThenInclude(x => x.TeamBans)
                .ToListAsync();
        }

        public async Task<bool> AddPlayer(Player player)
        {
            try
            {
                _context.Add(player);
                await _context.SaveChangesAsync();
                return true;
            }
            catch (Exception e) when (e is DbUpdateConcurrencyException ||
                                      e is DbUpdateException ||
                                      e is InvalidOperationException)
            {
                _logger.LogError(e, "Failed to add a player.");
                return false;
            }
        }

        public async Task<bool> UpdatePlayer(Player old, Player updated)
        {
            try
            {
                updated.Id = old.Id;
                _context.Entry(old).State = EntityState.Detached;
                _context.Update(updated);
                await _context.SaveChangesAsync();
                return true;
            }
            catch (Exception e) when (e is DbUpdateConcurrencyException ||
                                      e is DbUpdateException ||
                                      e is InvalidOperationException)
            {
                _logger.LogError(e, "Failed to update a player.");
                return false;
            }
        }

        public async Task<List<RankedQueueInfo>> GetRankedQueueInfosByPlayerId(string playerId)
        {
            return await _context.RankedQueueInfo.Where(x => x.PlayerId == playerId).ToListAsync();
        }

        public async Task<bool> AddRankedQueueInfo(RankedQueueInfo rqi)
        {
            try
            {
                await _context.AddAsync(rqi);
                await _context.SaveChangesAsync();
                return true;
            }
            catch (Exception e) when (e is DbUpdateConcurrencyException ||
                                      e is DbUpdateException ||
                                      e is InvalidOperationException)
            {
                _logger.LogError(e, "Failed to add player's ranked queue info.");
                return false;
            }
        }

        public async Task<bool> UpdateRankedQueueInfo(RankedQueueInfo old, RankedQueueInfo updated)
        {
            try
            {
                _context.Entry(old).State = EntityState.Detached;
                _context.Update(updated);
                await _context.SaveChangesAsync();
                return true;
            }
            catch (Exception e) when (e is DbUpdateConcurrencyException ||
                                      e is DbUpdateException ||
                                      e is InvalidOperationException)
            {
                _logger.LogError(e, "Failed to update player's ranked queue info.");
                return false;
            }
        }

        public async Task<List<PlayerMatch>> GetPlayerMatchesByPlayerId(string playerId)
        {
            return await _context.PlayerMatch.Where(x => x.PlayerId == playerId).ToListAsync();
        }

        public async Task<List<long>> GetPlayerMatchIdsByPlayerId(string playerId)
        {
            return await _context.PlayerMatch.Where(x => x.PlayerId == playerId).Select(x => x.GameId).ToListAsync();
        }

        public async Task<List<long>> GetMatchIdsInList(List<long> ids)
        {
            return await _context.Matches.Select(x => x.Id).Where(x => ids.Contains(x)).ToListAsync();
        }

        public async Task<bool> AddMatch(Match match)
        {
            try
            {
                await _context.AddAsync(match);
                await _context.SaveChangesAsync();
                return true;
            }
            catch (Exception e) when (e is DbUpdateConcurrencyException ||
                                      e is DbUpdateException ||
                                      e is InvalidOperationException)
            {
                _logger.LogError(e, "Failed to add match.");
                return false;
            }
        }

        public async Task<bool> AddPlayerMatch(PlayerMatch playerMatch)
        {
            try
            {
                await _context.AddAsync(playerMatch);
                await _context.SaveChangesAsync();
                return true;
            }
            catch (Exception e) when (e is DbUpdateConcurrencyException ||
                                      e is DbUpdateException ||
                                      e is InvalidOperationException)
            {
                _logger.LogError(e, "Failed to add player-match.");
                return false;
            }
        }

        public async Task<bool> AddTeamStatsRange(List<TeamStats> teamStats)
        {
            try
            {
                await _context.AddRangeAsync(teamStats);
                await _context.SaveChangesAsync();
                return true;
            }
            catch (Exception e) when (e is DbUpdateConcurrencyException ||
                                      e is DbUpdateException ||
                                      e is InvalidOperationException)
            {
                _logger.LogError(e, "Failed to add team stats.");
                return false;
            }
        }

        public async Task<bool> AddTeamBansRange(List<TeamBans> teamBans)
        {
            try
            {
                await _context.AddRangeAsync(teamBans);
                await _context.SaveChangesAsync();
                return true;
            }
            catch (Exception e) when (e is DbUpdateConcurrencyException || e is InvalidOperationException)
            {
                _logger.LogError(e, "Failed to add team bans.");
                return false;
            }
        }

        public async Task<bool> AddPlayerStatsRange(List<PlayerStats> playerStats)
        {
            try
            {
                await _context.AddRangeAsync(playerStats);
                await _context.SaveChangesAsync();
                return true;
            }
            catch (Exception e) when (e is DbUpdateConcurrencyException || e is InvalidOperationException)
            {
                _logger.LogError(e, "Failed to add player stats for match.");
                return false;
            }
        }

        //no use of this after UpdatePlayer method change, keeping for now
        public Player UpdatePlayerValues(Player existingPlayer, Player updatedPlayer)
        {
            existingPlayer.SummonerId = updatedPlayer.SummonerId;
            existingPlayer.AccountId = updatedPlayer.AccountId;
            existingPlayer.Name = updatedPlayer.Name;
            existingPlayer.Region = updatedPlayer.Region;
            existingPlayer.ProfileIconId = updatedPlayer.ProfileIconId;
            existingPlayer.SummonerLevel = updatedPlayer.SummonerLevel;

            return existingPlayer;
        }
    }
}