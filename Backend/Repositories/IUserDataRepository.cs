﻿using Backend.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Repositories
{
    public interface IUserDataRepository
    {
        Task<List<User>> GetUsers();
        Task<bool> AddUser(User user);
        Task<User> GetUser(string userId);
        Task<User> GetUserByUsername(string username);
        Task<User> GetUserByEmail(string email);
        Task<bool> FollowPlayer(UserPlayer follow);
        Task<bool> UnfollowPlayer(UserPlayer follow);
        Task<List<Player>> GetFavouritePlayers(User user);
    }
}
