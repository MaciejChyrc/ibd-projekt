using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Backend.Models;

namespace Backend.Repositories
{
    public interface IPlayerDataRepository
    {
        Task<Player> GetPlayerBySummonerNameAndRegion(string summonerName, string region);
        Task<Player> GetPlayerBySummonerIdAndRegion(long summonerId, string region);
        Task<Player> GetPlayerByAccountIdAndRegion(long accountId, string region);
        Task<List<Match>> GetRecentMatchesByPlayerId(string playerId, int count);
        Task<bool> AddPlayer(Player player);
        Task<bool> UpdatePlayer(Player old, Player updated);
        Task<List<RankedQueueInfo>> GetRankedQueueInfosByPlayerId(string playerId);
        Task<bool> AddRankedQueueInfo(RankedQueueInfo rqi);
        Task<bool> UpdateRankedQueueInfo(RankedQueueInfo old, RankedQueueInfo updated);
        Task<List<PlayerMatch>> GetPlayerMatchesByPlayerId(string playerId);
        Task<List<long>> GetPlayerMatchIdsByPlayerId(string playerId);
        Task<List<long>> GetMatchIdsInList(List<long> ids);
        Task<bool> AddMatch(Match match);
        Task<bool> AddPlayerMatch(PlayerMatch playerMatch);
        Task<bool> AddTeamStatsRange(List<TeamStats> teamStats);
        Task<bool> AddTeamBansRange(List<TeamBans> teamBans);
        Task<bool> AddPlayerStatsRange(List<PlayerStats> playerStats);
        Player UpdatePlayerValues(Player existingPlayer, Player updatedPlayer);
    }
}