using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Backend.Models;
using Backend.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using RestSharp;

//TODO: WIP as fuck, change used services and returned values, add endpoints
namespace Backend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PlayerDataController : ControllerBase
    {
        private readonly IPlayerDataService _playerDataService;
        private readonly ILogger<PlayerDataController> _logger;
        private readonly IHttpService _httpService; //added temporarily here, will be inside another service

        public PlayerDataController(IPlayerDataService playerDataService, ILogger<PlayerDataController> logger, IHttpService httpService)
        {
            _playerDataService = playerDataService;
            _logger = logger;
            _httpService = httpService;
        }

        [HttpGet("{region}/{summonerName}")]
        public async Task<IActionResult> GetPlayerInfo([FromRoute] string region, [FromRoute] string summonerName)
        {
            var playerResponse = await _playerDataService.GetPlayer(summonerName, region);

            if (playerResponse.Item2 == HttpStatusCode.OK)
                return Ok(playerResponse.Item1);
            else
                return NotFound();
        }

        [HttpPost]
        public async Task<IActionResult> RenewPlayerInfo([FromBody] SummonerNameRegion summonerNameRegion)
        {
            _logger.LogInformation("Action \"RenewPlayerInfo\": " + summonerNameRegion.SummonerName + " " + summonerNameRegion.Region);

            var playerResponse = await _playerDataService.FetchSavePlayer(summonerNameRegion.SummonerName, summonerNameRegion.Region);

            if (playerResponse.Item2 == HttpStatusCode.OK)
                return CreatedAtAction("GetPlayerInfo", new { region = summonerNameRegion.Region, summonerName = summonerNameRegion.SummonerName }, playerResponse.Item1);
            else return StatusCode((int)playerResponse.Item2);
        }

        [HttpGet("rankedInfo/{region}/{summonerId}")]
        public async Task<IActionResult> GetRankedQueueInfo([FromRoute] string region, [FromRoute] long summonerId)
        {
            var rankedQueueInfoResponse = await _playerDataService.GetPlayerRankedInfo(summonerId, region);

            if (rankedQueueInfoResponse.Item2 == HttpStatusCode.OK)
                return Ok(rankedQueueInfoResponse.Item1);
            else
                return NotFound();
        }

        [HttpPost("rankedInfo")]
        public async Task<IActionResult> RenewRankedQueueInfo([FromBody] SummonerIdRegion summonerIdRegion)
        {
            _logger.LogInformation("Action \"RenewRankedQueueInfo\": " + summonerIdRegion.SummonerId + " " + summonerIdRegion.Region);

            var rankedQueueInfoResponse = await _playerDataService.FetchPlayerRankedInfo(summonerIdRegion.SummonerId, summonerIdRegion.Region);

            if (rankedQueueInfoResponse.Item2 == HttpStatusCode.OK)
                return CreatedAtAction("GetRankedQueueInfo", new { region = summonerIdRegion.Region, summonerId = summonerIdRegion.SummonerId }, rankedQueueInfoResponse.Item1);
            else return StatusCode((int)rankedQueueInfoResponse.Item2);
        }

        [HttpGet("matches/{region}/{accountId}")]
        public async Task<IActionResult> GetRecentMatches([FromRoute] string region, [FromRoute] long accountId)
        {
            var matchesResponse = await _playerDataService.GetRecentMatches(accountId, region, 10);

            if (matchesResponse.Item2 == HttpStatusCode.OK)
                return Ok(matchesResponse.Item1);
            else
                return NotFound();
        }

        [HttpPost("matches")]
        public async Task<IActionResult> RenewRecentMatches([FromBody] AccountIdRegion accountIdRegion)
        {
            _logger.LogInformation("Action \"RenewRecentMatches\": " + accountIdRegion.AccountId + " " + accountIdRegion.Region);

            var matchesResponse = await _playerDataService.FetchRecentMatches(accountIdRegion.AccountId, accountIdRegion.Region, 10);

            if (matchesResponse.Item2 == HttpStatusCode.OK)
                return CreatedAtAction("GetRecentMatches", new { region = accountIdRegion.Region, accountId = accountIdRegion.AccountId }, matchesResponse.Item1);
            else return StatusCode((int)matchesResponse.Item2);
        }

        [HttpGet("getChallengers/{region}/{queue}")]
        public async Task<IActionResult> GetChallengerPlayers([FromRoute] string queue, [FromRoute] string region)
        {
            var response = await _httpService.GetChallengerLeagueByQueue(queue, region);

            if (response.Item2 == HttpStatusCode.OK)
                return Ok(response.Item1);
            else return StatusCode((int)response.Item2);
        }

        [HttpGet("liveMatch/{region}/{summonerId}")]
        public async Task<IActionResult> GetLiveMatch([FromRoute] string region, [FromRoute] long summonerId)
        {
            var response = await _playerDataService.GetLiveMatchData(summonerId, region);

            if (response.Item2 == HttpStatusCode.OK)
                return Ok(response.Item1);
            else return StatusCode((int)response.Item2);
        }
    }
}