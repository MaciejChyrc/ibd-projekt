﻿using Backend.Models;
using Backend.Models.HttpRequestBodyModels;
using Backend.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Backend.Controllers
{
    [Route("api/auth")]
    public class AuthController : Controller
    {
        private readonly IUserDataService _userDataService;
        private readonly IPlayerDataService _playerDataService;
        private readonly ILogger<AuthController> _logger;

        public AuthController(IUserDataService userDataService, IPlayerDataService playerDataService, ILogger<AuthController> logger, IHttpService httpService)
        {
            this._userDataService = userDataService;
            this._playerDataService = playerDataService;
            this._logger = logger;
        }

        // GET api/values
        [HttpPost, Route("login")]
        public IActionResult Login([FromBody]LoginModel user)
        {
            if (user == null)
            {
                return NotFound("There's no such user");
            }

            var loginUser = _userDataService.GetUserByUsername(user.UserName).Result;
            if (loginUser != null)
            {
                var secretKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("superSecretKey@345"));
                var signinCredentials = new SigningCredentials(secretKey, SecurityAlgorithms.HmacSha256);

                var tokenOptions = new JwtSecurityToken(
                    issuer: "http://localhost:5000",
                    audience: "http://localhost:5000",
                    claims: new List<Claim>(),
                    expires: DateTime.Now.AddMinutes(5),
                    signingCredentials: signinCredentials
                );

                var tokenString = new JwtSecurityTokenHandler().WriteToken(tokenOptions);
                if (loginUser.Password == user.Password)
                    return Ok(new { Token = tokenString });
                else
                    return Unauthorized();
            }
            else
            {
                return Unauthorized();
            }
        }

        [HttpPost, Route("register")]
        public IActionResult Register([FromBody]RegisterModel user)
        {
            if (user == null)
            {
                return BadRequest("Invalid client request");
            }

            if (_userDataService.GetUserByEmail(user.Email).Result != null || _userDataService.GetUserByUsername(user.UserName).Result != null)
            {
                return Conflict();
            }

            _userDataService.AddUser(user);
            return Ok();
        }

        [HttpPost, Route("follow"), Authorize]
        public async Task<IActionResult> FollowPlayer([FromBody]FollowModel followModel)
        {
            if (followModel == null)
            {
                return BadRequest("Invalid client request");
            }

            var user = await _userDataService.GetUserByUsername(followModel.UserName);
            var player = await _playerDataService.GetPlayer(followModel.SummonerName, followModel.Region);

            if (user == null || player.Item1 == null)
            {
                return BadRequest("This user or player not found");
            }

            var follow = new UserPlayer
            {
                UserId = user.Id,
                PlayerId = player.Item1.Id
            };

            await _userDataService.FollowPlayer(follow);
            return Ok();
        }

        [HttpPost, Route("unfollow"), Authorize]
        public async Task<IActionResult> UnfollowPlayer([FromBody]FollowModel followModel)
        {
            if (followModel == null)
            {
                return BadRequest("Invalid client request");
            }

            var user = await _userDataService.GetUserByUsername(followModel.UserName);
            var player = await _playerDataService.GetPlayer(followModel.SummonerName, followModel.Region);

            if (user == null || player.Item1 == null)
            {
                return NotFound("This user or player not found");
            }

            var follow = new UserPlayer
            {
                UserId = user.Id,
                PlayerId = player.Item1.Id
            };

            await _userDataService.UnfollowPlayer(follow);
            return Ok();
        }

        [HttpGet, Route("isfollowed"), Authorize]
        public IActionResult IsPlayerFollowed([FromBody]FollowModel followModel)
        {
            var myUser = _userDataService.GetUserByUsername(followModel.UserName).Result;
            if (myUser == null)
            {
                return BadRequest("User not found");
            }

            bool isFollowed = false;
            var players = _userDataService.GetFavouritePlayers(myUser).Result;
            foreach (var p in players)
            {
                if (p.Region == followModel.Region && p.Name == followModel.SummonerName)
                {
                    isFollowed = true;
                    break;
                }
            }

            return Ok(isFollowed);
        }

        [HttpGet, Route("favourite/{user}")]
        public async Task<IActionResult> GetFavouritePlayers([FromRoute] string user)
        {
            var myUser = await _userDataService.GetUserByUsername(user);
            if (myUser == null)
            {
                return NotFound("User not found");
            }
            var players = _userDataService.GetFavouritePlayers(myUser).Result;

            return Ok(players);
        }

        [HttpGet, Route("user")]
        public IActionResult GetUserByUsername(string username)
        {
            var myUser = _userDataService.GetUserByUsername(username).Result;
            if (myUser == null)
            {
                return BadRequest("User not found");
            }

            return Ok(myUser);
        }

    }
}
