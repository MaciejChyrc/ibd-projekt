using System;
using Backend.Models;
using Microsoft.EntityFrameworkCore;

namespace Backend.Data
{
    public class ApiDbContext : DbContext
    {
        public ApiDbContext(DbContextOptions<ApiDbContext> options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            builder.Entity<Player>().HasIndex(x => new { x.SummonerId, x.Region }).IsUnique();
            builder.Entity<RankedQueueInfo>().HasKey(x => new { x.PlayerId, x.QueueType });
            builder.Entity<TeamStats>().HasKey(x => new { x.GameId, x.TeamId });
            //why the hell can't I have a weak entity?
            builder.Entity<TeamBans>().HasKey(x => new { x.GameId, x.TeamId, x.ChampionId, x.PickTurn });
            builder.Entity<PlayerMatch>().HasKey(x => new { x.PlayerId, x.GameId });
            builder.Entity<UserPlayer>().HasKey(x => new { x.PlayerId, x.UserId });
            builder.Entity<PlayerStats>().HasKey(x => new { x.GameId, x.SummonerId, x.Region });
            builder.Entity<UserPlayer>().HasKey(x => new { x.UserId, x.PlayerId });
            builder.Entity<TeamBans>()
                   .HasOne<TeamStats>()
                   .WithMany(x => x.TeamBans)
                   .HasForeignKey(x => new { x.GameId, x.TeamId });
            builder.Entity<RankedQueueInfo>()
                   .HasOne<Player>()
                   .WithMany(x => x.RankedQueueInfo)
                   .HasForeignKey(x => x.PlayerId);
            builder.Entity<PlayerMatch>()
                   .HasOne<Player>()
                   .WithMany(x => x.Matches)
                   .HasForeignKey(x => x.PlayerId);
            builder.Entity<PlayerMatch>()
                   .HasOne<Match>(x => x.Game)
                   .WithMany()
                   .HasForeignKey(x => x.GameId);
            builder.Entity<TeamStats>()
                   .HasOne<Match>()
                   .WithMany(x => x.Teams)
                   .HasForeignKey(x => x.GameId);
            builder.Entity<PlayerStats>()
                   .HasOne<TeamStats>()
                   .WithMany(x => x.PlayerStats)
                   .HasForeignKey(x => new { x.GameId, x.TeamId });
            builder.Entity<UserPlayer>()
                   .HasOne<User>()
                   .WithMany()
                   .HasForeignKey(x => x.UserId);
            builder.Entity<UserPlayer>()
                   .HasOne<Player>()
                   .WithMany()
                   .HasForeignKey(x => x.PlayerId);
        }
        //TODO: add entities
        public DbSet<Player> Player { get; set; }
        public DbSet<RankedQueueInfo> RankedQueueInfo { get; set; }
        public DbSet<PlayerMatch> PlayerMatch { get; set; }
        public DbSet<Match> Matches { get; set; }
        public DbSet<TeamStats> TeamStats { get; set; }
        public DbSet<TeamBans> TeamBans { get; set; }
        public DbSet<User> User { get; set; }
        public DbSet<UserPlayer> UserPlayer { get; set; }
    }
}