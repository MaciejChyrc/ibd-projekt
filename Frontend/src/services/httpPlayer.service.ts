import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { LeagueList } from 'src/models/League';
import { LiveMatch } from 'src/models/LiveMatch';
import { Match } from 'src/models/Match';
import { Player } from 'src/models/Player';
import { RankedQueueInfo } from 'src/models/RankedQueueInfo';
import { environment } from '../environments/environment';
import { joinUrl } from '../utils/urlUtils';

@Injectable()
export class HttpPlayerService {
  private baseUrl: string = environment.baseUrl;

  constructor(private http: HttpClient) {}

  getPlayerInfo(region: string, summonerName: string): Observable<Player> {
    return this.http.get<Player>(
      joinUrl([this.baseUrl, 'api/PlayerData', region, summonerName])
    );
  }

  renewPlayerInfo(region: string, summonerName: string): Observable<Player> {
    const summonerNameRegion = {
      summonerName: summonerName,
      region: region
    };
    return this.http.post<Player>(
      joinUrl([this.baseUrl, 'api/PlayerData']),
      summonerNameRegion
    );
  }

  getRankedQueueInfo(
    region: string,
    summonerId: number
  ): Observable<RankedQueueInfo[]> {
    return this.http.get<RankedQueueInfo[]>(
      joinUrl([this.baseUrl, 'api/PlayerData/rankedInfo', region, summonerId])
    );
  }

  renewRankedQueueInfo(
    region: string,
    summonerId: number
  ): Observable<RankedQueueInfo[]> {
    const summonerIdRegion = {
      summonerId: summonerId,
      region: region
    };
    return this.http.post<RankedQueueInfo[]>(
      joinUrl([this.baseUrl, 'api/PlayerData/rankedInfo']),
      summonerIdRegion
    );
  }

  getRecentMatches(region: string, accountId: number): Observable<Match[]> {
    return this.http.get<Match[]>(
      joinUrl([this.baseUrl, 'api/PlayerData/matches', region, accountId])
    );
  }

  renewRecentMatches(region: string, accountId: number): Observable<Match[]> {
    const accountIdRegion = {
      accountId: accountId,
      region: region
    };
    return this.http.post<Match[]>(
      joinUrl([this.baseUrl, 'api/PlayerData/matches']),
      accountIdRegion
    );
  }

  getChallengers(region: string, queue: string): Observable<LeagueList> {
    return this.http.get<LeagueList>(
      joinUrl([this.baseUrl, 'api/PlayerData/getChallengers', region, queue])
    );
  }

  getLiveMatch(region: string, summonerId: number): Observable<LiveMatch> {
    return this.http.get<LiveMatch>(
      joinUrl([this.baseUrl, 'api/PlayerData/liveMatch', region, summonerId])
    );
  }
}
