import {
  HttpClient,
  HttpErrorResponse,
  HttpHeaders,
  HttpParams
} from '@angular/common/http';
import { Injectable, Input } from '@angular/core';
import * as moment from 'moment';
import { Observable, Subject } from 'rxjs';
import { Follow } from 'src/models/Follow';
import { Player } from 'src/models/Player';
import { User } from 'src/models/User';
import { environment } from '../environments/environment';
import { joinUrl } from '../utils/urlUtils';

@Injectable()
export class HttpUserService {
  private baseUrl: string = environment.baseUrl;
  private loggedInSubject: Subject<boolean>;
  loggedIn$: Observable<boolean>;
  private followedPlayersSubject: Subject<Player[]>;
  followedPlayers$: Observable<Player[]>;

  constructor(private http: HttpClient) {
    this.loggedInSubject = new Subject<boolean>();
    this.loggedIn$ = this.loggedInSubject.asObservable();
    this.followedPlayersSubject = new Subject<Player[]>();
    this.followedPlayers$ = this.followedPlayersSubject.asObservable();

    if (this.isLoggedIn()) {
      this.loggedInSubject.next(true);
      this.fetchFollowedPlayers().subscribe(players =>
        this.followedPlayersSubject.next(players)
      );
    } else {
      this.loggedInSubject.next(false);
      this.followedPlayersSubject.next([]);
    }
  }

  follow(player: Player) {
    const headers = new HttpHeaders({
      Authorization: 'Bearer ' + this.getToken()
    });

    console.log('Following user ' + localStorage.getItem('user'));
    const follow = {
      UserName: localStorage.getItem('user'),
      SummonerName: player.name,
      Region: player.region
    } as Follow;

    this.http
      .post<Player[]>(joinUrl([this.baseUrl, 'api/auth/follow']), follow, {
        headers: headers
      })
      .subscribe(
        next =>
          this.fetchFollowedPlayers().subscribe(players =>
            this.followedPlayersSubject.next(players)
          ),
        (error: HttpErrorResponse) =>
          console.log(error.status + ', ' + error.message)
      );
  }

  unfollow(player: Player) {
    const headers = new HttpHeaders({
      Authorization: 'Bearer ' + this.getToken()
    });
    console.log('Unfollowing user ' + localStorage.getItem('user'));
    const follow = {
      UserName: localStorage.getItem('user'),
      SummonerName: player.name,
      Region: player.region
    } as Follow;

    this.http
      .post<Player[]>(joinUrl([this.baseUrl, 'api/auth/unfollow']), follow, {
        headers: headers
      })
      .subscribe(
        next => this.fetchFollowedPlayers(),
        (error: HttpErrorResponse) =>
          console.log(error.status + ', ' + error.message)
      );
  }

  register(username: string, email: string, password: string) {
    return this.http.post<{ token: string }>(
      joinUrl([this.baseUrl, 'api/auth/register']),
      {
        username: username,
        email: email,
        password: password
      }
    );
  }

  login(username: string, password: string) {
    return this.http.post<{ token: string }>(
      joinUrl([this.baseUrl, 'api/auth/login']),
      {
        username: username,
        password: password
      }
    );
  }

  logout() {
    localStorage.removeItem('token');
    localStorage.removeItem('user');
    this.loggedInSubject.next(false);
    this.followedPlayersSubject.next([]);
  }

  onLogin(user: string, token: string) {
    localStorage.setItem('user', user);
    localStorage.setItem('token', token);
    this.loggedInSubject.next(true);
    this.fetchFollowedPlayers().subscribe(players =>
      this.followedPlayersSubject.next(players)
    );
  }

  isLoggedIn() {
    return !!localStorage.getItem('token') && !!localStorage.getItem('user');
  }

  getLoggedIn() {
    return this.loggedIn$;
  }

  setLoggedIn(value: boolean) {
    return this.loggedInSubject.next(value);
  }

  getToken() {
    return localStorage.getItem('token');
  }

  checkAccess() {
    const headers = new HttpHeaders({
      Authorization: 'Bearer ' + this.getToken()
    });

    const result = this.http.get<string>(
      joinUrl([this.baseUrl, 'api/values']),
      { headers: headers }
    );
    return result;
  }

  fetchFollowedPlayers() {
    return this.http.get<Player[]>(
      joinUrl([
        this.baseUrl,
        'api/auth/favourite',
        localStorage.getItem('user')
      ])
    );
  }
}
