import { KeyValue } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import orderBy from 'lodash-es/orderBy';
import { retry, timeout } from 'rxjs/operators';
import { Champion, ChampionFull, Spell } from 'src/models/Champion';
import { Rune, RuneTree } from 'src/models/Runes';
import { SummonerSpell } from 'src/models/SummonerSpells';
import { joinUrl } from 'src/utils/urlUtils';
import { DDRAGON_URLS } from '../constants/ddragon';

@Injectable()
export class StaticGameDataService {
  public versions: string[];
  public championDataList: ChampionFull[];
  public runes: RuneTree[];
  public summonerSpells: SummonerSpell[];

  constructor(private http: HttpClient) {
    this.init();
  }

  init() {
    this.getLolVersions().subscribe(
      versionData => {
        this.versions = versionData;
        this.getChampionDataList(this.versions[0], 'en_US').subscribe(
          champData => {
            this.championDataList = orderBy(
              this.convertFromObjectToArray(champData.data),
              'name',
              'asc'
            );
          },
          error => {
            console.error(error);
            this.championDataList = undefined;
          }
        );
        this.getRuneList(this.versions[0], 'en_US').subscribe(
          runeList => (this.runes = runeList),
          error => {
            console.error(error);
            this.runes = undefined;
          }
        );
        this.getSummonerspellList(this.versions[0], 'en_US').subscribe(
          summonerSpellData =>
            (this.summonerSpells = this.convertFromObjectToArray(
              summonerSpellData.data
            )),
          error => {
            console.error(error);
            this.summonerSpells = undefined;
          }
        );
      },
      error => {
        console.error(error);
        this.versions = [];
      }
    );
  }

  getLolVersions() {
    return this.http.get<string[]>(
      joinUrl([
        DDRAGON_URLS.baseUrl,
        DDRAGON_URLS.api,
        DDRAGON_URLS.versionsJson
      ])
    );
  }

  getChampionDataList(lolVersion: string, regional: string) {
    return this.http.get<any>(
      joinUrl([
        DDRAGON_URLS.baseUrl,
        DDRAGON_URLS.cdn,
        lolVersion,
        DDRAGON_URLS.data,
        DDRAGON_URLS.regionals.find(x => x === regional),
        DDRAGON_URLS.championJson
      ])
    );
  }

  getRuneList(lolVersion: string, regional: string) {
    return this.http.get<RuneTree[]>(
      joinUrl([
        DDRAGON_URLS.baseUrl,
        DDRAGON_URLS.cdn,
        lolVersion,
        DDRAGON_URLS.data,
        DDRAGON_URLS.regionals.find(x => x === regional),
        DDRAGON_URLS.runesReforgedJson
      ])
    );
  }

  getSummonerspellList(lolVersion: string, regional: string) {
    return this.http.get<any>(
      joinUrl([
        DDRAGON_URLS.baseUrl,
        DDRAGON_URLS.cdn,
        lolVersion,
        DDRAGON_URLS.data,
        DDRAGON_URLS.regionals.find(x => x === regional),
        DDRAGON_URLS.summonerSpellsJson
      ])
    );
  }

  convertFromObjectToArray(data) {
    const keyArray = Object.keys(data);
    const dataArray = [];
    keyArray.forEach((key: any) => {
      dataArray.push(data[key]);
    });

    return dataArray;
  }

  getChampionByChampionId(championId: number): ChampionFull {
    return this.championDataList.find(x => x.key === championId.toString());
  }

  getChampionByChampionName(championName: string): ChampionFull {
    return this.championDataList.find(x => x.name === championName);
  }

  getRuneById(runeId: number, treeId: number): Rune {
    const tree = this.getRuneTreeById(treeId);
    const runes = tree.slots.map(slot =>
      slot.runes.find(rune => rune.id === runeId)
    );
    return runes.find(rune => rune.id === runeId);
  }

  getRuneTreeById(runeId: number): RuneTree {
    return this.runes.find(tree => tree.id === runeId);
  }

  getSummonerspellBySummonerspellId(summonerSpellId: number): SummonerSpell {
    return this.summonerSpells.find(x => x.key === summonerSpellId.toString());
  }

  getProfileIconUrl(profileIconId: number) {
    return (
      joinUrl(
        [
          DDRAGON_URLS.baseUrl,
          DDRAGON_URLS.cdn,
          this.versions[0],
          DDRAGON_URLS.img,
          DDRAGON_URLS.profileIcon,
          profileIconId
        ]
      ) + '.png'
    );
  }

  getRankedTierIconUrl(tier: string): string {
    return './assets/tiers/' + tier.toLowerCase() + '.png';
  }

  getChampionSquareImageUrl(championId: number) {
    const championImageFilename = this.getChampionByChampionId(championId).image
      .full;

    return joinUrl([
      DDRAGON_URLS.baseUrl,
      DDRAGON_URLS.cdn,
      this.versions[0],
      DDRAGON_URLS.img,
      DDRAGON_URLS.champion,
      championImageFilename
    ]);
  }

  getChampionSplashArtUrl(champion: string, skin: number) {
    return joinUrl([
      DDRAGON_URLS.baseUrl,
      DDRAGON_URLS.cdn,
      DDRAGON_URLS.img,
      DDRAGON_URLS.champion,
      DDRAGON_URLS.splash,
      champion + '_' + skin + '.jpg'
    ]);
  }

  getRuneTreeImageUrl(runeId: number) {
    const runeFilename = this.getRuneTreeById(runeId).icon;

    return joinUrl([
      DDRAGON_URLS.baseUrl,
      DDRAGON_URLS.cdn,
      DDRAGON_URLS.img,
      runeFilename
    ]);
  }

  getRuneImageUrl(runeId: number, treeId: number) {
    const runeFilename = this.getRuneById(runeId, treeId).icon;

    return joinUrl([
      DDRAGON_URLS.baseUrl,
      DDRAGON_URLS.cdn,
      DDRAGON_URLS.img,
      runeFilename
    ]);
  }

  getSummonerSpellImageUrl(summonerSpellId: number) {
    const summonerSpellImageFilename = this.getSummonerspellBySummonerspellId(
      summonerSpellId
    ).image.full;

    return joinUrl([
      DDRAGON_URLS.baseUrl,
      DDRAGON_URLS.cdn,
      this.versions[0],
      DDRAGON_URLS.img,
      DDRAGON_URLS.spell,
      summonerSpellImageFilename
    ]);
  }

  getItemImageUrl(itemId: number) {
    return (
      joinUrl(
        [
          DDRAGON_URLS.baseUrl,
          DDRAGON_URLS.cdn,
          this.versions[0],
          DDRAGON_URLS.img,
          DDRAGON_URLS.item,
          itemId
        ]
      ) + '.png'
    );
  }

  getSpellImageUrl(spell: Spell) {
    const spellImageFilename = spell.image.full;

    return joinUrl([
      DDRAGON_URLS.baseUrl,
      DDRAGON_URLS.cdn,
      this.versions[0],
      DDRAGON_URLS.img,
      DDRAGON_URLS.spell,
      spellImageFilename
    ]);
  }

  getPassiveImageUrl(spell: Spell) {
    const spellImageFilename = spell.image.full;

    return joinUrl([
      DDRAGON_URLS.baseUrl,
      DDRAGON_URLS.cdn,
      this.versions[0],
      DDRAGON_URLS.img,
      DDRAGON_URLS.passive,
      spellImageFilename
    ]);
  }

  getAllChampionsIdNames(): KeyValue<string, string>[] {
    const map: KeyValue<string, string>[] = [];
    this.championDataList.forEach(x => map.push({ key: x.key, value: x.name }));
    return map;
  }
}
