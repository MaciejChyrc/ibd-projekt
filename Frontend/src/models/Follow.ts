export interface Follow {
  UserName: string;
  SummonerName: string;
  Region: string;
}
