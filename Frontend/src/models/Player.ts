export interface Player {
  id: string;
  summonerId: number;
  accountId: number;
  name: string;
  region: string;
  profileIconId: number;
  summonerLevel: number;
}
