export interface RankedQueueInfo {
  playerId: string;
  playerOrTeamId: string;
  playerOrTeamName: string;
  leagueId: string;
  leagueName: string;
  tier: string;
  rank: string;
  leaguePoints: number;
  queueType: string;
  wins: number;
  losses: number;
  hotStreak: boolean;
  freshBlood: boolean;
  veteran: boolean;
  inactive: boolean;
  promoSeriesWins?: number;
  promoSeriesLosses?: number;
  promoSeriesTarget?: number;
  promoSeriesProgress?: string;
}
