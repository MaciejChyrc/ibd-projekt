import { PlayerStats } from './PlayerStats';
import { TeamBans } from './TeamBans';

export interface TeamStats {
  teamId: number;
  teamBans: TeamBans[];
  playerStats: PlayerStats[];
  win: string;
  firstBlood: boolean;
  firstTower: boolean;
  firstDragon: boolean;
  firstRiftHerald: boolean;
  firstBaron: boolean;
  firstInhibitor: boolean;
  towerKills: number;
  dragonKills: number;
  riftHeraldKills: number;
  baronKills: number;
  inhibitorkills: number;
  vilemawKills: number;
  dominionVictoryScore: number;
}
