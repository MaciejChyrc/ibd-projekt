export interface LeagueList {
  leagueId: string;
  tier: string;
  entries: LeagueItem[];
  queue: string;
  name: string;
}

export interface LeagueItem {
  rank: string;
  hotStreak: boolean;
  miniSeries: MiniSeries;
  wins: number;
  losses: number;
  veteran: boolean;
  freshBlood: boolean;
  playerOrTeamName: string;
  inactive: boolean;
  playerOrTeamId: string;
  leaguePoints: number;
}

export interface MiniSeries {
  wins: number;
  losses: number;
  target: number;
  progress: string;
}
