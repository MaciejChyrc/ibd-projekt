import { Player } from "./Player";

export interface User {
    id: string;
    UserName: string;    
    Email: string;
    EmailConfirmed: string;
    Role: string;
    Password: string;
    FavouritePlayers: Player[];
  }
  