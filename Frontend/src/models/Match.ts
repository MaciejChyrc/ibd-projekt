import { TeamStats } from './TeamStats';

export interface Match {
  id: number;
  teams: TeamStats[];
  queue: number;
  season: number;
  gameVersion: string;
  gameMode: string;
  gameType: string;
  mapId: number;
  timestamp: number;
  gameDuration: number;
}
