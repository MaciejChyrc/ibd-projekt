import { Player } from './Player';
import { PlayerStats } from './PlayerStats';
import { RankedQueueInfo } from './RankedQueueInfo';

export interface LiveMatch {
  gameId: number;
  gameStartTime: number;
  gameMode: string;
  mapId: number;
  gameType: string;
  queueType: number;
  observerKey: string;
  bannedChampions: BannedChampion[];
  participants: Participant[];
}

export interface Participant {
  championId: number;
  teamId: number;
  spell1Id: number;
  spell2Id: number;
  player: Player;
  rankedQueueInfos: RankedQueueInfo[];
  lastGamesStats: PlayerStats[];
}

export interface BannedChampion {
  teamId: number;
  championId: number;
  pickTurn: number;
}
