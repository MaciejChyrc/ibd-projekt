import { DDRAGON_URLS } from '../constants/ddragon';
import * as urlUtils from './urlUtils';

describe('urlUtils tests', () => {
  it('should return correct url', () => {
    const urlPieces: (string | number | undefined)[] = [
      DDRAGON_URLS.baseUrl,
      DDRAGON_URLS.cdn,
      DDRAGON_URLS.img,
      DDRAGON_URLS.profileIcon,
      '001.png'
    ];
    expect(urlUtils.joinUrl(urlPieces)).toBe(
      'https://ddragon.leagueoflegends.com/cdn/img/profileicon/001.png'
    );
  });
});
