export function joinUrl(pieces: (string | number | undefined)[]): string {
  return pieces.filter(x => x.toString()).join('/');
}
