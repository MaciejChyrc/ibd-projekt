import { Pipe, PipeTransform } from '@angular/core';
import { RANKED_QUEUE_TYPES_VIEW_MAP } from 'src/constants/queueTypes';

@Pipe({ name: 'rankedQueueTypePipe' })
export class RankedQueueTypePipe implements PipeTransform {
  transform(queueType: string): string {
    return RANKED_QUEUE_TYPES_VIEW_MAP.find(x => x.key === queueType).value;
  }
}
