export const DDRAGON_URLS = {
  baseUrl: 'https://ddragon.leagueoflegends.com',
  api: 'api',
  cdn: 'cdn',
  data: 'data',
  img: 'img',
  item: 'item',
  versionsJson: 'versions.json',
  championJson: 'championFull.json',
  runesReforgedJson: 'runesReforged.json',
  summonerSpellsJson: 'summoner.json',
  profileIcon: 'profileicon',
  champion: 'champion',
  spell: 'spell',
  passive: 'passive',
  splash: 'splash',
  regionals: ['en_US']
};
