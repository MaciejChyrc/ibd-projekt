import { KeyValue } from '@angular/common';

export const REGIONS: KeyValue<string, string>[] = [
  { key: 'br1', value: 'BR' },
  { key: 'eun1', value: 'EUNE' },
  { key: 'euw1', value: 'EUW' },
  { key: 'jp1', value: 'JP' },
  { key: 'kr', value: 'KR' },
  { key: 'la1', value: 'LAN' },
  { key: 'la2', value: 'LAS' },
  { key: 'na1', value: 'NA' },
  { key: 'oc1', value: 'OCE' },
  { key: 'tr1', value: 'TR' },
  { key: 'ru', value: 'RU' },
  { key: 'pbe1', value: 'PBE' }
];
