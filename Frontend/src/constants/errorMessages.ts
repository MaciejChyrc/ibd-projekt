export const PLAYER_FIND_ERROR: { [code: number]: string } = {
  404: `Can't find a player with given summoner name.`,
  422: `The searched player hasn't played since match history collection has begun.`,
  429: `Exceeded request limit, please wait a moment before retrying.`,
  500: `Received an error while searching for a player.`,
  504: `Can't contact Riot Games services.`
};
