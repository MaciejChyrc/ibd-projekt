import { KeyValue } from '@angular/common';

export const RANKED_QUEUE_TYPES_VIEW_MAP: KeyValue<string, string>[] = [
  { key: 'RANKED_SOLO_5x5', value: 'Ranked Solo 5v5' },
  { key: 'RANKED_FLEX_SR', value: 'Ranked Flex 5v5' },
  { key: 'RANKED_FLEX_TT', value: 'Ranked Flex 3v3' }
];

interface MapQueueType {
  map: string;
  queue: string;
}

enum Maps {
  CUSTOM_GAME = 'Custom game',
  SUMMONERS_RIFT = 'Summoner\'s Rift',
  TWISTED_TREELINE = 'Twisted Treeline',
  HOWLING_ABYSS = 'Howling Abyss',
  CRYSTAL_SCAR = 'Crystal Scar',
  BUTCHERS_BRIDGE = 'Butcher\'s Bridge',
  COSMIC_RUINS = 'Cosmic Ruins',
  VALORAN_CITY_PARK = 'Valoran City Park',
  OVERCHARGE = 'Overcharge',
  CRASH_SITE = 'Crash Site',
  NEXUS_BLITZ = 'Nexus Blitz'
}

export const ALL_MAPS_QUEUE_TYPES: KeyValue<number, MapQueueType>[] = [
  { key: 0, value: { map: Maps.CUSTOM_GAME, queue: '' } },
  {
    key: 72,
    value: { map: Maps.HOWLING_ABYSS, queue: 'Snowdown Showdown 1v1' }
  },
  {
    key: 73,
    value: { map: Maps.HOWLING_ABYSS, queue: 'Snowdown Showdown 2v2' }
  },
  { key: 75, value: { map: Maps.SUMMONERS_RIFT, queue: 'Hexakill' } },
  { key: 76, value: { map: Maps.SUMMONERS_RIFT, queue: 'URF' } },
  { key: 78, value: { map: Maps.HOWLING_ABYSS, queue: 'One for All: Mirror' } },
  { key: 83, value: { map: Maps.SUMMONERS_RIFT, queue: 'Coop vs AI URF' } },
  { key: 98, value: { map: Maps.TWISTED_TREELINE, queue: 'Hexakill' } },
  { key: 100, value: { map: Maps.BUTCHERS_BRIDGE, queue: 'ARAM' } },
  { key: 310, value: { map: Maps.SUMMONERS_RIFT, queue: 'Nemesis' } },
  {
    key: 313,
    value: { map: Maps.SUMMONERS_RIFT, queue: 'Black Market Brawlers' }
  },
  {
    key: 317,
    value: { map: Maps.CRYSTAL_SCAR, queue: 'Definitely Not Dominion' }
  },
  { key: 325, value: { map: Maps.SUMMONERS_RIFT, queue: 'All Random' } },
  { key: 400, value: { map: Maps.SUMMONERS_RIFT, queue: 'Normal Draft 5v5' } },
  { key: 420, value: { map: Maps.SUMMONERS_RIFT, queue: 'Ranked Solo 5v5' } },
  { key: 430, value: { map: Maps.SUMMONERS_RIFT, queue: 'Blind Pick 5v5' } },
  { key: 440, value: { map: Maps.SUMMONERS_RIFT, queue: 'Ranked Flex 5v5' } },
  { key: 450, value: { map: Maps.HOWLING_ABYSS, queue: 'ARAM' } },
  { key: 460, value: { map: Maps.TWISTED_TREELINE, queue: 'Blind Pick 3v3' } },
  { key: 470, value: { map: Maps.TWISTED_TREELINE, queue: 'Ranked Flex 3v3' } },
  { key: 600, value: { map: Maps.SUMMONERS_RIFT, queue: 'Blood Hunt' } },
  {
    key: 610,
    value: { map: Maps.COSMIC_RUINS, queue: 'Dark Star: Singularity' }
  },
  { key: 700, value: { map: Maps.SUMMONERS_RIFT, queue: 'Clash' } },
  {
    key: 800,
    value: { map: Maps.TWISTED_TREELINE, queue: 'Coop vs AI Intermediate' }
  },
  {
    key: 810,
    value: { map: Maps.TWISTED_TREELINE, queue: 'Coop vs AI Intro' }
  },
  {
    key: 820,
    value: { map: Maps.TWISTED_TREELINE, queue: 'Coop vs AI Beginner' }
  },
  { key: 830, value: { map: Maps.SUMMONERS_RIFT, queue: 'Coop vs AI Intro' } },
  {
    key: 840,
    value: { map: Maps.SUMMONERS_RIFT, queue: 'Coop vs AI Beginner' }
  },
  {
    key: 850,
    value: { map: Maps.SUMMONERS_RIFT, queue: 'Coop vs AI Intermediate' }
  },
  { key: 900, value: { map: Maps.SUMMONERS_RIFT, queue: 'ARURF' } },
  { key: 910, value: { map: Maps.CRYSTAL_SCAR, queue: 'Ascension' } },
  {
    key: 920,
    value: { map: Maps.HOWLING_ABYSS, queue: 'Legend of the Poro King' }
  },
  { key: 940, value: { map: Maps.SUMMONERS_RIFT, queue: 'Nexus Siege' } },
  { key: 950, value: { map: Maps.SUMMONERS_RIFT, queue: 'Doom Bots Voting' } },
  { key: 960, value: { map: Maps.SUMMONERS_RIFT, queue: 'Doom Bots' } },
  {
    key: 980,
    value: {
      map: Maps.VALORAN_CITY_PARK,
      queue: 'Star Guardian Invasion: Normal'
    }
  },
  {
    key: 990,
    value: {
      map: Maps.VALORAN_CITY_PARK,
      queue: 'Star Guardian Invasion: Onslaught'
    }
  },
  { key: 1000, value: { map: Maps.OVERCHARGE, queue: 'PROJECT: Hunters' } },
  { key: 1010, value: { map: Maps.SUMMONERS_RIFT, queue: 'Snow ARURF' } },
  { key: 1020, value: { map: Maps.SUMMONERS_RIFT, queue: 'One for All' } },
  {
    key: 1030,
    value: { map: Maps.CRASH_SITE, queue: 'Odyssey Extraction: Intro' }
  },
  {
    key: 1040,
    value: { map: Maps.CRASH_SITE, queue: 'Odyssey Extraction: Cadet' }
  },
  {
    key: 1050,
    value: { map: Maps.CRASH_SITE, queue: 'Odyssey Extraction: Crewmember' }
  },
  {
    key: 1060,
    value: { map: Maps.CRASH_SITE, queue: 'Odyssey Extraction: Captain' }
  },
  {
    key: 1070,
    value: { map: Maps.CRASH_SITE, queue: 'Odyssey Extraction: Onslaught' }
  },
  { key: 1200, value: { map: Maps.NEXUS_BLITZ, queue: 'Nexus Blitz' } }
];
