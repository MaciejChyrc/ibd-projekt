import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { HttpUserService } from 'src/services/httpUser.service';

@Component({
  selector: 'register-form',
  templateUrl: './register.component.html'
})
export class RegisterComponent implements OnInit {
  message: string = undefined;
  formItems = this.fb.group({
    login: ['', Validators.required],
    email: ['', [Validators.email, Validators.required]],
    password: ['', Validators.required]
  });

  constructor(
    private fb: FormBuilder,
    private http: HttpUserService,
    private dialogRef: MatDialogRef<RegisterComponent>
  ) {}

  ngOnInit(): void {}

  onSubmit() {
    this.http
      .register(
        this.formItems.get('login').value,
        this.formItems.get('email').value,
        this.formItems.get('password').value
      )
      .subscribe(
        next => (this.message = 'Successfully created an account'),
        error => (this.message = 'Failed to create an account')
      );
  }
}
