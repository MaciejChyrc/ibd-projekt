import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { HttpUserService } from 'src/services/httpUser.service';

@Component({
  selector: 'login-form',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  message: string = undefined;
  formItems = this.fb.group({
    login: ['', Validators.required],
    password: ['', Validators.required]
  });

  constructor(
    private fb: FormBuilder,
    private http: HttpUserService,
    private dialogRef: MatDialogRef<LoginComponent>
  ) {}

  ngOnInit(): void {}

  onSubmit() {
    const login = this.formItems.get('login').value;
    const password = this.formItems.get('password').value;
    this.http.login(login, password).subscribe(
      next => {
        this.http.onLogin(login, next.token);
        this.message = 'Logged in successfully';
      },
      (error: HttpErrorResponse) =>
        (this.message = `Failed to log in (${error.status})`)
    );
  }
}
