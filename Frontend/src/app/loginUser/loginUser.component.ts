import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { HttpUserService } from 'src/services/httpUser.service';
import { LoginComponent } from './components/login.component';
import { RegisterComponent } from './components/register.component';

@Component({
  selector: 'login-user',
  templateUrl: 'loginUser.component.html'
})
export class LoginUserComponent implements OnInit {
  loggedIn: boolean;
  constructor(private http: HttpUserService, private dialog: MatDialog) {
    http.getLoggedIn().subscribe(loggedIn => {
      this.loggedIn = loggedIn;
      console.log('Login status: ' + loggedIn);
    });
  }

  ngOnInit() {
    this.http.getLoggedIn().subscribe(loggedIn => {
      this.loggedIn = loggedIn;
      console.log('Login status: ' + loggedIn);
    });
  }

  logOut() {
    this.http.logout();
  }

  openLoginDialog() {
    const dialogRef = this.dialog.open(LoginComponent);
  }

  openRegisterDialog() {
    const dialogRef = this.dialog.open(RegisterComponent);
  }
}
