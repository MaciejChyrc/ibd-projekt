import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { REGIONS } from 'src/constants/regions';
import { HttpPlayerService } from 'src/services/httpPlayer.service';

@Component({
  selector: 'search-summoner',
  templateUrl: 'searchSummoner.component.html'
})
export class SearchSummonerComponent implements OnInit {
  regions = REGIONS;
  formItems = this.fb.group({
    summonerName: [''],
    region: [this.regions[0].key]
  });

  constructor(private fb: FormBuilder, private router: Router) {}

  ngOnInit() {}

  onSubmit() {
    this.goToSummoner(
      this.formItems.get('region').value,
      this.formItems.get('summonerName').value
    );
  }

  goToSummoner(region: string, summonerName: string) {
    this.router.navigate(['summoner', region, summonerName]);
  }
}
