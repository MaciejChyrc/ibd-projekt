import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import orderBy from 'lodash-es/orderBy';
import { LeagueItem, LeagueList } from 'src/models/League';
import { Player } from 'src/models/Player';
import { HttpPlayerService } from 'src/services/httpPlayer.service';
import { StaticGameDataService } from 'src/services/staticGameData.service';

@Component({
  selector: 'top-players',
  templateUrl: './topPlayers.component.html',
  styleUrls: ['./topPlayers.component.scss']
})
export class TopPlayersComponent implements OnInit {
  topPlayer: Player;
  region: string;
  queue: string;
  league: LeagueList;
  displayedColumns = ['position', 'name', 'league', 'lp', 'winRatio'];
  fetching = false;

  constructor(
    private http: HttpPlayerService,
    private staticGameData: StaticGameDataService,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.route.params.subscribe((params: Params) => {
      this.region = params['region'];
      this.queue = params['queue'];
      this.fetchLeague(this.region, this.queue);
    });
  }

  fetchLeague(region: string, queue: string) {
    this.fetching = true;
    this.http.getChallengers(region, queue).subscribe(
      data => {
        data.entries = orderBy(data.entries, 'leaguePoints', 'desc');
        this.league = data;
      },
      (error: HttpErrorResponse) => console.error(error),
      () => {
        this.fetching = false;
        console.log(this.league);
        this.fetchPlayer(region, this.league.entries[0].playerOrTeamName);
      }
    );
  }

  fetchPlayer(region: string, summonerName: string) {
    this.http.getPlayerInfo(region, summonerName).subscribe(
      data => (this.topPlayer = data),
      (getError: HttpErrorResponse) => {
        getError.status === 404
          ? this.http
              .renewPlayerInfo(region, summonerName)
              .subscribe(
                data => (this.topPlayer = data),
                (postError: HttpErrorResponse) => console.error(postError)
              )
          : console.error(getError);
      }
    );
  }

  getWinRatio(player: LeagueItem) {
    return ((player.wins / (player.wins + player.losses)) * 100).toFixed(0);
  }
}
