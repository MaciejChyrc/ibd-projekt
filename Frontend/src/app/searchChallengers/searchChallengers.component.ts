import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { RANKED_QUEUE_TYPES_VIEW_MAP } from 'src/constants/queueTypes';
import { REGIONS } from 'src/constants/regions';

@Component({
  selector: 'search-challengers',
  templateUrl: './searchChallengers.component.html',
  styleUrls: ['./searchChallengers.component.scss']
})
export class SearchChallengersComponent implements OnInit {
  regions = REGIONS;
  queues = RANKED_QUEUE_TYPES_VIEW_MAP;
  formItems = this.fb.group({
    queue: [this.queues[0].key],
    region: [this.regions[0].key]
  });

  constructor(private fb: FormBuilder, private router: Router) {}

  ngOnInit(): void {}

  onSubmit() {
    this.goToChallengers(
      this.formItems.get('region').value,
      this.formItems.get('queue').value
    );
  }

  goToChallengers(region: string, queue: string) {
    this.router.navigate(['top-players', region, queue]);
  }
}
