import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PLAYER_FIND_ERROR } from 'src/constants/errorMessages';

@Component({
  selector: 'home',
  templateUrl: 'home.component.html',
  styleUrls: ['home.component.scss']
})
export class HomeComponent implements OnInit {
  funFact: string;
  errorMessage?: string;

  constructor(private route: ActivatedRoute) {}

  ngOnInit() {
    const error = this.route.snapshot.params['errorCode'];
    this.errorMessage = PLAYER_FIND_ERROR[error] || undefined;
    console.log(error);
    console.log(this.errorMessage);
    this.funFact = `If the app doesn't work it's jungler's fault.`;
  }
}
