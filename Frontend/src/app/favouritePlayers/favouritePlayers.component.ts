import { Component, OnInit } from '@angular/core';
import { interval, Observable, Subscription } from 'rxjs';
import { Player } from 'src/models/Player';
import { HttpUserService } from 'src/services/httpUser.service';

@Component({
  selector: 'fav-players',
  templateUrl: './favouritePlayers.component.html',
  styleUrls: ['./favouritePlayers.component.scss']
})
export class FavouritePlayersComponent implements OnInit {
  followedPlayers: Player[];

  constructor(private httpUser: HttpUserService) {}

  ngOnInit(): void {
    this.httpUser
      .fetchFollowedPlayers()
      .subscribe(players => (this.followedPlayers = players));
  }
}
