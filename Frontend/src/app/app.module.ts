import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule, Routes } from '@angular/router';

import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatDialogModule } from '@angular/material/dialog';
import { MatDividerModule } from '@angular/material/divider';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatMenuModule } from '@angular/material/menu';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSelectModule } from '@angular/material/select';
import { MatTableModule } from '@angular/material/table';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatTooltipModule } from '@angular/material/tooltip';
import { ChartsModule } from 'ng2-charts';

import { AppComponent } from './app.component';
import { ChampionInfoComponent } from './champions/championInfo/championInfo.component';
import { ChampionsListComponent } from './champions/championsList.component';
import { FavouritePlayersComponent } from './favouritePlayers/favouritePlayers.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './loginUser/components/login.component';
import { RegisterComponent } from './loginUser/components/register.component';
import { LoginUserComponent } from './loginUser/loginUser.component';
import { NavbarComponent } from './navbar/navbar.component';
import { TopPlayersComponent } from './rankings/ladder/topPlayers.component';
import { SearchChallengersComponent } from './searchChallengers/searchChallengers.component';
import { SearchSummonerComponent } from './searchSummoner/searchSummoner.component';
import { LiveMatchComponent } from './summonerInfo/components/liveMatch/liveMatch.component';
import { MatchPanelComponent } from './summonerInfo/components/matchesList/components/matchPanel.component';
import { MatchesListComponent } from './summonerInfo/components/matchesList/matchesList.component';
import { KdaChartComponent } from './summonerInfo/components/playerPerformance/components/kdaChart/kdaChart.component';
// tslint:disable-next-line:max-line-length
import { MostPlayedChampsComponent } from './summonerInfo/components/playerPerformance/components/mostPlayedChamps/mostPlayedChamps.component';
import { WinRateDoughnutComponent } from './summonerInfo/components/playerPerformance/components/winRateDoughnut/winRateDoughnut.component';
import { PlayerPerformanceComponent } from './summonerInfo/components/playerPerformance/playerPerformance.component';
import { PlayerSummaryComponent } from './summonerInfo/components/playerSummary/playerSummary.component';
import { RankedInfoComponent } from './summonerInfo/components/rankedInfo/rankedInfo.component';
import { SummonerInfoComponent } from './summonerInfo/summonerInfo.component';

import { HttpPlayerService } from 'src/services/httpPlayer.service';
import { HttpUserService } from 'src/services/httpUser.service';
import { StaticGameDataService } from 'src/services/staticGameData.service';
import { RankedQueueTypePipe } from 'src/utils/rankedQueueTypePipe';

const routes: Routes = [
  { path: '', redirectTo: 'search', pathMatch: 'full' },
  { path: 'search', component: HomeComponent },
  { path: 'search/:errorCode', component: HomeComponent },
  { path: 'summoner/:region/:summonerName', component: SummonerInfoComponent },
  { path: 'top-players/:region/:queue', component: TopPlayersComponent },
  { path: 'champions', component: ChampionsListComponent },
  { path: 'champions/:champion', component: ChampionInfoComponent },
  { path: 'fav-players', component: FavouritePlayersComponent },
  { path: '**', redirectTo: 'search' }
];

@NgModule({
  declarations: [
    AppComponent,
    ChampionInfoComponent,
    ChampionsListComponent,
    HomeComponent,
    NavbarComponent,
    SearchChallengersComponent,
    SearchSummonerComponent,
    SummonerInfoComponent,
    RankedInfoComponent,
    PlayerSummaryComponent,
    PlayerPerformanceComponent,
    MatchesListComponent,
    MatchPanelComponent,
    LiveMatchComponent,
    TopPlayersComponent,
    WinRateDoughnutComponent,
    KdaChartComponent,
    MostPlayedChampsComponent,
    RankedQueueTypePipe,
    LoginUserComponent,
    LoginComponent,
    RegisterComponent,
    FavouritePlayersComponent
  ],
  entryComponents: [LoginComponent, RegisterComponent],
  imports: [
    BrowserModule,
    ChartsModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatCardModule,
    MatDialogModule,
    MatDividerModule,
    MatExpansionModule,
    MatFormFieldModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatMenuModule,
    MatProgressSpinnerModule,
    MatSelectModule,
    MatTableModule,
    MatToolbarModule,
    MatTooltipModule,
    RouterModule.forRoot(routes)
  ],
  providers: [HttpPlayerService, StaticGameDataService, HttpUserService],
  bootstrap: [AppComponent]
})
export class AppModule {}
