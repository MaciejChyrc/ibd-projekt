import {
  Component,
  Input,
  OnChanges,
  OnInit,
  SimpleChanges
} from '@angular/core';
import flatten from 'lodash-es/flatten';
import { Match } from 'src/models/Match';
import { PlayerStats } from 'src/models/PlayerStats';

@Component({
  selector: 'player-performance',
  templateUrl: './playerPerformance.component.html',
  styleUrls: ['./playerPerformance.component.scss']
})
export class PlayerPerformanceComponent implements OnInit {
  @Input() matches: Match[];
  @Input() summonerId: number;
  @Input() region: string;
  playerStatsList: PlayerStats[];

  constructor() {}

  ngOnInit(): void {
    this.init();
  }

  // tslint:disable-next-line:use-life-cycle-interface
  ngOnChanges(changes: SimpleChanges): void {
    this.init();
  }

  init() {
    const temp: PlayerStats[] = [];
    this.matches.forEach(x => {
      temp.push(this.findPlayer(x, this.region, this.summonerId));
    });
    this.playerStatsList = temp;
  }

  findPlayer(match: Match, region: string, summonerId: number): PlayerStats {
    return flatten(match.teams.map(x => x.playerStats)).find(
      x => x.region === region && x.summonerId === summonerId
    );
  }
}
