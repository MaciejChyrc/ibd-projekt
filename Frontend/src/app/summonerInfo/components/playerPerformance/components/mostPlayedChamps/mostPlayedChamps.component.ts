import { KeyValue } from '@angular/common';
import { Component, Input, OnInit, SimpleChanges } from '@angular/core';
import orderBy from 'lodash-es/orderBy';
import { PlayerStats } from 'src/models/PlayerStats';
import { StaticGameDataService } from 'src/services/staticGameData.service';

@Component({
  selector: 'most-played-champs',
  templateUrl: './mostPlayedChamps.component.html',
  styleUrls: ['./mostPlayedChamps.component.scss']
})
export class MostPlayedChampsComponent implements OnInit {
  @Input() playerStatsList: PlayerStats[];
  mostPlayedChampsStats: ChampRecentGameStats[];

  constructor(private staticGameData: StaticGameDataService) {}

  ngOnInit(): void {
    this.initData(this.playerStatsList);
  }

  // tslint:disable-next-line:use-life-cycle-interface
  ngOnChanges(changes: SimpleChanges): void {
    this.initData(this.playerStatsList);
  }

  getMostPlayedChamps(playerStatsList: PlayerStats[]) {
    const temp: KeyValue<number, number>[] = [];

    playerStatsList.forEach(x => {
      if (temp.find(y => y.key === x.championId)) {
        temp.find(y => y.key === x.championId).value++;
      } else {
        temp.push({ key: x.championId, value: 1 });
      }
    });

    return orderBy(temp, 'value', 'desc').slice(0, 3);
  }

  initData(playerStatsList: PlayerStats[]) {
    const topChampsPlayedGames = this.getMostPlayedChamps(playerStatsList);
    const mostPlayedChampsStats: ChampRecentGameStats[] = [];

    topChampsPlayedGames.forEach(x => {
      const temp: ChampRecentGameStats = {
        championId: x.key,
        gamesPlayed: x.value,
        wins: playerStatsList
          .filter(y => y.championId === x.key)
          .map(y => y.win)
          .filter(y => y === true).length,
        losses: playerStatsList
          .filter(y => y.championId === x.key)
          .map(y => y.win)
          .filter(y => y === false).length,
        kills: this.getKillCount(playerStatsList, x.key),
        deaths: this.getDeathCount(playerStatsList, x.key),
        assists: this.getAssistCount(playerStatsList, x.key)
      };

      mostPlayedChampsStats.push(temp);
    });

    this.mostPlayedChampsStats = mostPlayedChampsStats;
  }

  getKillCount(playerStatsList: PlayerStats[], championId: number) {
    return playerStatsList
      .filter(x => x.championId === championId)
      .map(x => x.kills)
      .reduce((x, y) => x + y, 0);
  }

  getDeathCount(playerStatsList: PlayerStats[], championId: number) {
    return playerStatsList
      .filter(x => x.championId === championId)
      .map(x => x.deaths)
      .reduce((x, y) => x + y, 0);
  }

  getAssistCount(playerStatsList: PlayerStats[], championId: number) {
    return playerStatsList
      .filter(x => x.championId === championId)
      .map(x => x.assists)
      .reduce((x, y) => x + y, 0);
  }

  getKda(champStats: ChampRecentGameStats) {
    return (champStats.kills + champStats.assists) / champStats.deaths;
  }

  getKdaColor(champStats: ChampRecentGameStats) {
    const kda = this.getKda(champStats);

    if (kda < 2.0) {
      return 'rgba(237, 74, 74, 1)';
    } else if (kda >= 2.0 && kda < 3.0) {
      return 'rgba(74, 144, 237, 1)';
    } else {
      return 'rgba(86, 204, 103, 1)';
    }
  }

  getWinRateColor(champStats: ChampRecentGameStats) {
    const winRate = champStats.wins / champStats.gamesPlayed;

    if (winRate < 0.45) {
      return 'rgba(237, 74, 74, 1)';
    } else if (winRate >= 0.45 && winRate < 0.55) {
      return 'rgba(74, 144, 237, 1)';
    } else {
      return 'rgba(86, 204, 103, 1)';
    }
  }
}

interface ChampRecentGameStats {
  championId: number;
  gamesPlayed: number;
  wins: number;
  losses: number;
  kills: number;
  deaths: number;
  assists: number;
}
