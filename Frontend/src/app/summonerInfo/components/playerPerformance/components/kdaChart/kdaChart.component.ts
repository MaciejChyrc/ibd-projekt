import { Component, Input, OnInit, SimpleChanges } from '@angular/core';
import { PlayerStats } from 'src/models/PlayerStats';

@Component({
  selector: 'kda-chart',
  templateUrl: './kdaChart.component.html',
  styleUrls: ['./kdaChart.component.scss']
})
export class KdaChartComponent implements OnInit {
  @Input() playerStatsList: PlayerStats[];
  avgKills: number;
  avgDeaths: number;
  avgAssists: number;
  kda: number;
  kdaSeparatedChartData: any[];
  kdaSeparatedChartColors = [
    {
      backgroundColor: 'rgba(86, 204, 103, 0.3)',
      borderColor: 'rgba(86, 204, 103, 1)'
    },
    {
      backgroundColor: 'rgba(237, 74, 74, 0.3)',
      borderColor: 'rgba(237, 74, 74, 1)'
    },
    {
      backgroundColor: 'rgba(74, 144, 237, 0.3)',
      borderColor: 'rgba(74, 144, 237, 1)'
    }
  ];
  kdaCombinedChartColors = [
    {
      backgroundColor: 'rgba(74, 144, 237, 0.3)',
      borderColor: 'rgba(74, 144, 237, 1)'
    }
  ];
  kdaCombinedChartData: any[];
  kdaChartLabels: number[];
  chartType = 'line';
  chosenChart: 'separated' | 'combined';

  constructor() {}

  ngOnInit(): void {
    this.initData(this.playerStatsList);
  }

  // tslint:disable-next-line:use-life-cycle-interface
  ngOnChanges(changes: SimpleChanges): void {
    this.initData(this.playerStatsList);
  }

  initData(playerStatsList: PlayerStats[]) {
    playerStatsList = playerStatsList.reverse();
    this.chosenChart = 'separated';
    this.avgKills = this.getKillCount(playerStatsList) / playerStatsList.length;
    this.avgDeaths =
      this.getDeathCount(playerStatsList) / playerStatsList.length;
    this.avgAssists =
      this.getAssistCount(playerStatsList) / playerStatsList.length;
    this.kda = (this.avgKills + this.avgAssists) / this.avgDeaths;
    this.kdaChartLabels = Array.from(
      { length: playerStatsList.length },
      (x, y) => y + 1
    );
    this.kdaSeparatedChartData = [
      { data: playerStatsList.map(x => x.kills), label: 'Kills' },
      { data: playerStatsList.map(x => x.deaths), label: 'Deaths' },
      { data: playerStatsList.map(x => x.assists), label: 'Assists' }
    ];
    this.kdaCombinedChartData = [
      {
        data: playerStatsList.map(x => (x.kills + x.assists) / x.deaths),
        label: 'KDA'
      }
    ];
  }

  getKillCount(playerStatsList: PlayerStats[]) {
    return playerStatsList.map(x => x.kills).reduce((x, y) => x + y, 0);
  }

  getDeathCount(playerStatsList: PlayerStats[]) {
    return playerStatsList.map(x => x.deaths).reduce((x, y) => x + y, 0);
  }

  getAssistCount(playerStatsList: PlayerStats[]) {
    return playerStatsList.map(x => x.assists).reduce((x, y) => x + y, 0);
  }

  getKdaColor() {
    if (this.kda < 2.0) {
      return 'rgba(237, 74, 74, 1)';
    } else if (this.kda >= 2.0 && this.kda < 3.0) {
      return 'rgba(74, 144, 237, 1)';
    } else {
      return 'rgba(86, 204, 103, 1)';
    }
  }

  toggleChart() {
    this.chosenChart =
      this.chosenChart === 'separated' ? 'combined' : 'separated';
  }
}
