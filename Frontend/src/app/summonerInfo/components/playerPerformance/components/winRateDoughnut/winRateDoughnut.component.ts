import { Component, Input, OnInit, SimpleChanges } from '@angular/core';
import { PlayerStats } from 'src/models/PlayerStats';

@Component({
  selector: 'winrate-doughnut',
  templateUrl: './winRateDoughnut.component.html',
  styleUrls: ['./winRateDoughnut.component.scss']
})
export class WinRateDoughnutComponent implements OnInit {
  @Input() playerStatsList: PlayerStats[];
  winRate: number;
  wins: number;
  losses: number;
  chartLabels: string[];
  chartData: number[];
  chartType = 'doughnut';
  chartColors: any[] = [
    {
      backgroundColor: ['rgba(129, 204, 140, 0.7)', 'rgba(209, 125, 125, 0.7)'],
      borderColor: ['rgba(129, 204, 140, 1)', 'rgba(209, 125, 125, 1)']
    }
  ];

  constructor() {}

  ngOnInit(): void {
    this.initData(this.playerStatsList);
  }

  // tslint:disable-next-line:use-life-cycle-interface
  ngOnChanges(changes: SimpleChanges): void {
    this.initData(this.playerStatsList);
  }

  initData(playerStatsList: PlayerStats[]) {
    this.wins = playerStatsList.filter(x => x.win === true).length;
    this.losses = playerStatsList.length - this.wins;
    this.winRate = this.wins / (this.wins + this.losses);
    this.chartLabels = ['wins', 'losses'];
    this.chartData = [this.wins, this.losses];
  }
}
