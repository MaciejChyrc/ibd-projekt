import { Component, Input, OnInit } from '@angular/core';
import { Match } from 'src/models/Match';

@Component({
  selector: 'matches-list',
  templateUrl: './matchesList.component.html',
  styleUrls: ['./matchesList.component.scss']
})
export class MatchesListComponent implements OnInit {
  @Input() matches: Match[];
  @Input() summonerId: number;
  @Input() region: string;
  constructor() {}

  ngOnInit(): void {}
}
