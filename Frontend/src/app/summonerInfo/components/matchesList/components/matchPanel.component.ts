import { Component, Input, OnInit } from '@angular/core';
import { flatten } from 'lodash-es';
import * as moment from 'moment';
import { ALL_MAPS_QUEUE_TYPES } from 'src/constants/queueTypes';
import { Match } from 'src/models/Match';
import { PlayerStats } from 'src/models/PlayerStats';
import { TeamStats } from 'src/models/TeamStats';
import { StaticGameDataService } from 'src/services/staticGameData.service';

@Component({
  selector: 'match-panel',
  templateUrl: './matchPanel.component.html',
  styleUrls: ['./matchPanel.component.scss']
})
export class MatchPanelComponent implements OnInit {
  @Input() match: Match;
  @Input() summonerId: number;
  @Input() region: string;

  playerStats: PlayerStats;
  durationFormatted: string;

  constructor(private staticGameData: StaticGameDataService) {}

  ngOnInit(): void {
    this.findPlayer(this.region, this.summonerId);
    this.durationFormatted = moment
      .utc(moment.duration(this.match.gameDuration * 1000).asMilliseconds())
      .format('mm:ss');
  }

  findPlayer(region: string, summonerId: number) {
    this.match.teams.forEach(team => {
      team.playerStats.forEach(player => {
        if (player.region === region && player.summonerId === summonerId) {
          this.playerStats = player;
        }
      });
    });
  }

  getMapQueue(queueType: number) {
    return ALL_MAPS_QUEUE_TYPES.find(x => x.key === queueType).value;
  }

  getTeamKills(team: TeamStats) {
    return team.playerStats.map(x => x.kills).reduce((x, y) => x + y, 0);
  }

  getPlayerTeamKills(playerStats: PlayerStats) {
    const team = this.match.teams.find(
      x => x.playerStats.filter(y => y === playerStats).length > 0
    );
    return this.getTeamKills(team);
  }

  getMaxTeamKills() {
    return Math.max(
      ...this.match.teams.map(x => {
        return this.getTeamKills(x);
      })
    );
  }

  getKillsMaxPercent(kills: number) {
    return this.getMaxTeamKills()
      ? ((kills / this.getMaxTeamKills()) * 100).toFixed(0)
      : 0;
  }

  getMaxTowerKills() {
    return Math.max(...this.match.teams.map(x => x.towerKills));
  }

  getTowerKillsMaxPercent(towersKilled: number) {
    return this.getMaxTowerKills()
      ? ((towersKilled / this.getMaxTowerKills()) * 100).toFixed(0)
      : 0;
  }

  getMaxDragonKills() {
    return Math.max(...this.match.teams.map(x => x.dragonKills));
  }

  getDragonKillsMaxPercent(dragonsKilled: number) {
    return this.getMaxDragonKills()
      ? ((dragonsKilled / this.getMaxDragonKills()) * 100).toFixed(0)
      : 0;
  }

  getMaxBaronKills() {
    return Math.max(...this.match.teams.map(x => x.baronKills));
  }

  getBaronKillsMaxPercent(baronsKilled: number) {
    return this.getMaxBaronKills()
      ? ((baronsKilled / this.getMaxBaronKills()) * 100).toFixed(0)
      : 0;
  }

  getTeamWin(win: string) {
    return win.toUpperCase() === 'WIN' ? true : false;
  }

  getHighestDamageDealt(): number {
    return Math.max(
      ...flatten(this.match.teams.map(x => x.playerStats)).map(
        x => x.totalDamageDealtToChampions
      )
    );
  }

  getDmgMaxPercent(dmg: number) {
    return ((dmg / this.getHighestDamageDealt()) * 100).toFixed(0);
  }

  getHighestMultikill(playerStats: PlayerStats): string {
    return playerStats.pentaKills
      ? 'Pentakill'
      : playerStats.quadraKills
      ? 'Quadra kill'
      : playerStats.tripleKills
      ? 'Triple kill'
      : playerStats.doubleKills
      ? 'Double kill'
      : '';
  }

  getBgColor(win: boolean): string {
    return win ? '#81cc8c' : '#d17d7d';
  }

  getExpandedBgColor(win: boolean): string {
    return win ? '#9adbb9' : '#db9a9a';
  }

  getBorderColor(win: boolean): string {
    return win ? '#35b047' : '#d14d4d';
  }
}
