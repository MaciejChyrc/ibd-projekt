import { Component, Input, OnInit } from '@angular/core';
import { RankedQueueInfo } from 'src/models/RankedQueueInfo';
import { StaticGameDataService } from 'src/services/staticGameData.service';

@Component({
  selector: 'ranked-info',
  templateUrl: './rankedInfo.component.html',
  styleUrls: ['./rankedInfo.component.scss']
})
export class RankedInfoComponent implements OnInit {
  @Input() rankedInfo: RankedQueueInfo[];
  constructor(private staticGameData: StaticGameDataService) {}

  ngOnInit(): void {}
}
