import { KeyValue } from '@angular/common';
import { Component, Input, OnInit } from '@angular/core';
import sortBy from 'lodash-es/sortBy';
import { ALL_MAPS_QUEUE_TYPES } from 'src/constants/queueTypes';
import { LiveMatch, Participant } from 'src/models/LiveMatch';
import { PlayerStats } from 'src/models/PlayerStats';
import { RankedQueueInfo } from 'src/models/RankedQueueInfo';
import { HttpPlayerService } from 'src/services/httpPlayer.service';
import { StaticGameDataService } from 'src/services/staticGameData.service';

@Component({
  selector: 'live-match',
  templateUrl: './liveMatch.component.html',
  styleUrls: ['./liveMatch.component.scss']
})
export class LiveMatchComponent implements OnInit {
  @Input() summonerId: number;
  @Input() region: string;
  liveMatch: LiveMatch;
  teamsWinPoints: KeyValue<number, number>[];
  fetching = false;

  constructor(
    private http: HttpPlayerService,
    private staticGameData: StaticGameDataService
  ) {}

  ngOnInit(): void {
    this.fetching = true;
    this.fetchLiveMatch(this.region, this.summonerId).subscribe(
      data => {
        console.log(data);
        this.liveMatch = data;
        this.getTeamsWinPoints(data);
      },
      error => {
        console.error(error);
        this.fetching = false;
      },
      () => (this.fetching = false)
    );
  }

  fetchLiveMatch(region: string, summonerId: number) {
    return this.http.getLiveMatch(region, summonerId);
  }

  getTeams(liveMatch: LiveMatch) {
    return new Set(liveMatch.participants.map(x => x.teamId));
  }

  getTeamPlayers(teamId: number, liveMatch: LiveMatch) {
    return liveMatch.participants.filter(x => x.teamId === teamId);
  }

  getTeamsWinPoints(liveMatch: LiveMatch) {
    const teamIds = new Set(liveMatch.participants.map(x => x.teamId));
    const teamsWinPoints: KeyValue<number, number>[] = [];

    teamIds.forEach(x => {
      teamsWinPoints.push({
        key: x,
        value: this.getTeamWinPoints(x, liveMatch)
      });
    });
    console.log(teamsWinPoints);

    this.teamsWinPoints = teamsWinPoints;
  }

  getTeamWinPoints(teamId: number, liveMatch: LiveMatch) {
    const team = liveMatch.participants.filter(x => x.teamId === teamId);
    let winRatePoints = 0;
    let kdaPoints = 0;
    let rankPoints = 0;
    team.forEach(x => {
      if (x.lastGamesStats) {
        winRatePoints += this.getParticipantWinRate(x);
        kdaPoints += this.getParticipantKda(x);
      }
      rankPoints += this.getRankValue(x);
    });
    return winRatePoints * 3 + kdaPoints + rankPoints;
  }

  getKillCount(playerStatsList: PlayerStats[]) {
    return playerStatsList.map(x => x.kills).reduce((x, y) => x + y, 0);
  }

  getDeathCount(playerStatsList: PlayerStats[]) {
    return playerStatsList.map(x => x.deaths).reduce((x, y) => x + y, 0);
  }

  getAssistCount(playerStatsList: PlayerStats[]) {
    return playerStatsList.map(x => x.assists).reduce((x, y) => x + y, 0);
  }

  getParticipantKda(participant: Participant) {
    const kills = this.getKillCount(participant.lastGamesStats);
    const deaths = this.getDeathCount(participant.lastGamesStats);
    const assists = this.getAssistCount(participant.lastGamesStats);
    let kda = (kills + assists) / deaths;
    if (!isFinite(kda)) {
      kda = 10.0;
    }
    return kda;
  }

  getParticipantWinRate(participant: Participant) {
    return (
      participant.lastGamesStats.filter(x => x.win === true).length /
      participant.lastGamesStats.length
    );
  }

  getRankValue(participant: Participant) {
    if (participant.rankedQueueInfos.length === 0) {
      return 0;
    }

    const sorted = this.getSortedRanks(participant);

    if (sorted[0].tier.toUpperCase() === 'BRONZE') {
      return 0;
    } else if (sorted[0].tier.toUpperCase() === 'SILVER') {
      return 1;
    } else if (sorted[0].tier.toUpperCase() === 'GOLD') {
      return 2;
    } else if (sorted[0].tier.toUpperCase() === 'PLATINUM') {
      return 3;
    } else if (sorted[0].tier.toUpperCase() === 'DIAMOND') {
      return 4;
    } else if (sorted[0].tier.toUpperCase() === 'MASTER') {
      return 5;
    } else if (sorted[0].tier.toUpperCase() === 'CHALLENGER') {
      return 6;
    } else {
      return 0;
    }
  }

  getSortedRanks(participant: Participant) {
    return sortBy<RankedQueueInfo>(
      participant.rankedQueueInfos,
      this.sortByQueue
    );
  }

  sortByQueue(ri: RankedQueueInfo) {
    const rank = {
      RANKED_SOLO_5x5: 1,
      RANKED_FLEX_SR: 2,
      RANKED_FLEX_TT: 3
    };
    return rank[ri.queueType];
  }

  getTeamBans(teamId: number) {
    return this.liveMatch.bannedChampions
      .filter(x => x.teamId === teamId)
      .map(x => x.championId);
  }

  getTeamWinChance(teamId: number) {
    const teamPoints = this.teamsWinPoints
      .filter(x => x.key === teamId)
      .map(x => x.value)[0];
    const enemyPoints = this.teamsWinPoints
      .filter(x => x.key !== teamId)
      .map(x => x.value)[0];
    return teamPoints / (teamPoints + enemyPoints);
  }

  getMapQueue(queueType: number) {
    return ALL_MAPS_QUEUE_TYPES.find(x => x.key === queueType).value;
  }

  getWinChanceBgColor(teamId: number) {
    const chance = this.getTeamWinChance(teamId);

    if (chance > 0.51) {
      return 'rgba(86, 204, 103, 1)';
    } else if (chance > 0.49 && chance <= 0.51) {
      return '#000';
    } else {
      return 'rgba(237, 74, 74, 1)';
    }
  }
}
