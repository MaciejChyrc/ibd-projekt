import {
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  SimpleChanges
} from '@angular/core';
import { Player } from 'src/models/Player';
import { HttpUserService } from 'src/services/httpUser.service';
import { StaticGameDataService } from 'src/services/staticGameData.service';

@Component({
  selector: 'player-summary',
  templateUrl: './playerSummary.component.html',
  styleUrls: ['./playerSummary.component.scss']
})
export class PlayerSummaryComponent implements OnInit {
  @Input() player: Player;
  @Input() followed: boolean;
  @Output() refreshDataEvent = new EventEmitter();
  @Output() toggleLiveMatchEvent = new EventEmitter();
  @Output() toggleFollowEvent = new EventEmitter();

  constructor(
    private staticGameData: StaticGameDataService,
    private http: HttpUserService
  ) {}

  ngOnInit(): void {}

  // tslint:disable-next-line:use-life-cycle-interface
  ngOnChanges(changes: SimpleChanges): void {
    this.followed = changes.followed.currentValue;
  }

  refreshData() {
    this.refreshDataEvent.next();
  }

  toggleLiveMatch() {
    this.toggleLiveMatchEvent.next();
  }

  toggleFollowPlayer() {
    this.toggleFollowEvent.next();
  }
}
