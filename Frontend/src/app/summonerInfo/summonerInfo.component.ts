import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import orderBy from 'lodash-es/orderBy';
import sortBy from 'lodash-es/sortBy';
import { Match } from 'src/models/Match';
import { Player } from 'src/models/Player';
import { RankedQueueInfo } from 'src/models/RankedQueueInfo';
import { HttpPlayerService } from 'src/services/httpPlayer.service';
import { HttpUserService } from 'src/services/httpUser.service';

@Component({
  selector: 'summoner-info',
  templateUrl: './summonerInfo.component.html',
  styleUrls: ['./summonerInfo.component.scss']
})
export class SummonerInfoComponent implements OnInit {
  player: Player;
  rankedInfo: RankedQueueInfo[];
  matches: Match[];
  summonerName: string;
  region: string;
  followed: boolean;
  liveMatchOpen = false;

  constructor(
    private http: HttpPlayerService,
    private httpUser: HttpUserService,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.route.params.subscribe((params: Params) => {
      this.summonerName = params['summonerName'];
      this.region = params['region'];
      this.fetchPlayer(this.region, this.summonerName);
    });

    console.log(
      'region:' + this.region + ', summonerName: ' + this.summonerName
    );
  }

  fetchPlayer(region: string, summonerName: string) {
    this.http.getPlayerInfo(region, summonerName).subscribe(
      data => (this.player = data),
      (getError: HttpErrorResponse) => {
        getError.status === 404
          ? this.http.renewPlayerInfo(region, summonerName).subscribe(
              data => (this.player = data),
              (postError: HttpErrorResponse) => {
                console.error(postError);
                this.goToSearchWithError(postError.status);
              },
              () => (
                this.fetchRankedInfo(region, this.player.summonerId),
                this.fetchMatches(region, this.player.accountId),
                this.isPlayerFollowed()
              )
            )
          : (console.error(getError),
            this.goToSearchWithError(getError.status));
      },
      () => (
        this.fetchRankedInfo(region, this.player.summonerId),
        this.fetchMatches(region, this.player.accountId),
        this.isPlayerFollowed()
      )
    );
  }

  fetchRankedInfo(region: string, summonerId: number) {
    this.http.getRankedQueueInfo(region, summonerId).subscribe(
      data =>
        (this.rankedInfo = sortBy<RankedQueueInfo>(data, this.sortByQueue)),
      (getError: HttpErrorResponse) => {
        getError.status === 404
          ? this.renewRankedInfo(region, summonerId)
          : console.error(getError);
      }
    );
  }

  fetchMatches(region: string, accountId: number) {
    this.http.getRecentMatches(region, accountId).subscribe(
      data => (this.matches = orderBy<Match>(data, 'timestamp', 'desc')),
      (getError: HttpErrorResponse) => {
        getError.status === 404
          ? this.renewMatches(region, accountId)
          : console.error(getError);
      }
    );
  }

  renewPlayer(region: string, summonerName: string) {
    this.http.renewPlayerInfo(region, summonerName).subscribe(
      data => (this.player = data),
      (postError: HttpErrorResponse) => {
        console.error(postError);
        this.goToSearchWithError(postError.status);
      },
      () => (
        this.renewRankedInfo(region, this.player.summonerId),
        this.renewMatches(region, this.player.accountId)
      )
    );
  }

  renewRankedInfo(region: string, summonerId: number) {
    this.http.renewRankedQueueInfo(region, summonerId).subscribe(
      data =>
        (this.rankedInfo = sortBy<RankedQueueInfo>(data, this.sortByQueue)),
      (postError: HttpErrorResponse) => {
        console.error(postError);
      }
    );
  }

  renewMatches(region: string, accountId: number) {
    this.http.renewRecentMatches(region, accountId).subscribe(
      data => (this.matches = orderBy<Match>(data, 'timestamp', 'desc')),
      (postError: HttpErrorResponse) => {
        console.error(postError);
      }
    );
  }

  goToSearchWithError(statusCode: number) {
    this.router.navigate(['search', statusCode]);
  }

  sortByQueue(ri: RankedQueueInfo) {
    const rank = {
      RANKED_SOLO_5x5: 1,
      RANKED_FLEX_SR: 2,
      RANKED_FLEX_TT: 3
    };
    return rank[ri.queueType];
  }

  onRefreshData() {
    this.renewPlayer(this.region, this.summonerName);
  }

  onLiveMatchToggle() {
    this.liveMatchOpen = !this.liveMatchOpen;
  }

  onFollowToggle() {
    if (!this.followed) {
      this.httpUser.follow(this.player);
    } else {
      this.httpUser.unfollow(this.player);
    }
  }

  isPlayerFollowed() {
    this.httpUser.followedPlayers$.subscribe(
      data => {
        this.followed =
          data.filter(
            x => x.name.toUpperCase() === this.player.name.toUpperCase()
          ).length > 0;
      },
      error => console.error(error)
    );
  }
}
