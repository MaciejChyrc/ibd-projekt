import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { ChampionFull } from 'src/models/Champion';
import { StaticGameDataService } from 'src/services/staticGameData.service';

@Component({
  selector: 'champion-info',
  templateUrl: './championInfo.component.html',
  styleUrls: ['./championInfo.component.scss']
})
export class ChampionInfoComponent implements OnInit {
  championId: string;
  champion: ChampionFull;

  constructor(
    private staticGameData: StaticGameDataService,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.route.params.subscribe((params: Params) => {
      this.championId = params['champion'];
      console.log(this.championId);
      // bottleneck :( TODO: fetch data different way so it's more lightweight
      this.staticGameData.getLolVersions().subscribe(versions => {
        this.staticGameData.getChampionDataList(versions[0], 'en_US').subscribe(
          data => {
            this.champion = data.data[this.championId];
          },
          error => console.error(error),
          () => console.log(this.champion)
        );
      });
    });
  }
}
