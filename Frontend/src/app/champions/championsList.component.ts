import { Component, OnInit } from '@angular/core';
import { StaticGameDataService } from 'src/services/staticGameData.service';

@Component({
  selector: 'champions-list',
  templateUrl: './championsList.component.html',
  styleUrls: ['./championsList.component.scss']
})
export class ChampionsListComponent implements OnInit {
  constructor(private staticGameData: StaticGameDataService) {}

  ngOnInit(): void {}
}
